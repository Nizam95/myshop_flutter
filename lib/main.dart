import 'package:catcher/core/catcher.dart';
import 'package:myshop_flutter/app.dart';
import 'package:myshop_flutter/src/constants/app_constant.dart';
import 'package:myshop_flutter/src/constants/enum_constant.dart';
import 'package:myshop_flutter/src/services/flavor_service.dart';
import 'init.dart';

void main() async {
  await init();

  //Setup environment
  FlavorService(
      flavor: Flavor.production,
      values: FlavorValues(
        baseUrl: "https://yourapi", //required
        url: "yourapi", //required
//        imageUrl: "image url", //optional
//        addressUrl: "address url", //optional
        reCaptchaKey: "your recaptcha key", //google recaptcha KEY
      ));

  //initialize app
  Catcher(
    App(),
    debugConfig: CatcherConfig.debugOptions,
    releaseConfig: CatcherConfig.releaseOptions,
  );
}
