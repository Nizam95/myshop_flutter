import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:myshop_flutter/src/controllers/auth_controller.dart';
import 'package:myshop_flutter/src/controllers/localization_controller.dart';
import 'package:get_storage/get_storage.dart';
import 'locator.dart';

Future<void> init() async {
  WidgetsFlutterBinding.ensureInitialized();
  try {
    ///INITIALIZE ALL GLOBAL SINGLETON HERE
    await setupLocator();
    await GetStorage.init();
    await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

    ///INITIALIZE ALL GLOBAL GET CONTROLLER HERE
    Get.put<LocalizationController>(LocalizationController());
    Get.put<AuthController>(AuthController());
  } catch (e) {
    print(e.toString());
  }
}
