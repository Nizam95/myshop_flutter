import 'package:catcher/core/catcher.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:myshop_flutter/src/constants/app_constant.dart';
import 'package:myshop_flutter/src/constants/app_data.dart';
import 'package:myshop_flutter/src/constants/app_theme_constant.dart';
import 'package:myshop_flutter/src/constants/assets_path.dart';
import 'package:myshop_flutter/src/constants/route_path.dart';

import 'package:myshop_flutter/src/controllers/localization_controller.dart';
import 'package:myshop_flutter/src/routes/router.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      navigatorKey: Catcher.navigatorKey,
      theme: ThemeData(
        primaryColor: AppTheme.primaryAppColor,
        accentColor: AppTheme.accentAppColor,
      ),
      title: AppConstant.AppName,
      locale: LocalizationController.getInstance.getLocale,
      localizationsDelegates: [
        FlutterI18nDelegate(
          translationLoader: FileTranslationLoader(
              basePath: AssetsPath.TranslationPath,
              fallbackFile: DefaultData.defaultLanguage,
              useCountryCode: false),
          missingTranslationHandler: (key, locale) {
            print(
                "--- Missing Key: $key, languageCode: ${locale.languageCode}");
          },
        ),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: StaticListData.languageOptions,
//      navigatorObservers: [
//        FirebaseAnalyticsObserver(analytics: FirebaseAnalytics()),
//      ],
      debugShowCheckedModeBanner: false,
      initialRoute: RoutePaths.SplashScreenPage,
      getPages: Router.routesSetting,
    );
  }
}
