import 'dart:async';
import 'dart:io';

import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:get/get.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

import 'package:myshop_flutter/src/constants/app_theme_constant.dart';
import 'package:myshop_flutter/src/constants/assets_path.dart';
import 'package:myshop_flutter/src/widgets/button/fab_button_widget.dart';
import 'package:myshop_flutter/src/widgets/dialog_widget.dart';
import 'package:myshop_flutter/src/widgets/snackbar_widget.dart';

class PinCodePage extends StatefulWidget {
  final String phoneNumber;
  final String successMessage;

  PinCodePage({this.phoneNumber, this.successMessage});
  @override
  _PinCodePageState createState() => _PinCodePageState();
}

class _PinCodePageState extends State<PinCodePage> {
  var onTapRecognizer;

  TextEditingController textEditingController = TextEditingController();
  StreamController<ErrorAnimationType> errorController;
  bool hasError = false;
  bool isShowingSnackBar = false;
  bool _isLoading = false;
  String pinNo = "";
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    onTapRecognizer = TapGestureRecognizer()..onTap = submitPhoneNumber;
    errorController = StreamController<ErrorAnimationType>();
  }

  @override
  void dispose() {
    errorController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      key: scaffoldKey,
      body: ModalProgressHUD(
        inAsyncCall: _isLoading,
        child: GestureDetector(
          onTap: () {
            Get.focusScope.unfocus();
          },
          child: Container(
            decoration: AppTheme.gradientBackground,
            height: Get.height,
            width: Get.width,
            child: ListView(
              children: <Widget>[
                FabButtonWidget(
                  onPressed: () {
                    Get.focusScope.unfocus();
                    Get.back();
                  },
                ),
                Container(
                  height: MediaQuery.of(context).size.height / 3.2,
                  child: FlareActor(
                    AssetsPath.PhoneFlare,
                    animation: "otp",
                    fit: BoxFit.fitHeight,
                    alignment: Alignment.topCenter,
                  ),
                ),
                SizedBox(height: 4),
                Padding(
                  padding: EdgeInsets.symmetric(
                      vertical: AppTheme.paddingVerticalSmall),
                  child: Text(
                    FlutterI18n.translate(
                        context, "text_label.phoneNumberVerification"),
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: AppTheme.bigTitleFontSize),
                    textAlign: TextAlign.center,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: AppTheme.paddingHorizontalExtraBig + 4,
                      vertical: AppTheme.paddingVerticalSmall),
                  child: RichText(
                    text: TextSpan(
                        text:
                            '${FlutterI18n.translate(context, "text_label.enterCodeSent")} ',
                        children: [
                          TextSpan(
                              text: '+${widget.phoneNumber}',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: AppTheme.subtitleFontSize)),
                        ],
                        style: TextStyle(
                            color: Colors.black54,
                            fontSize: AppTheme.subtitleFontSize)),
                    textAlign: TextAlign.center,
                  ),
                ),
                AppTheme.dividerVerticalSmall,
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: AppTheme.paddingHorizontalExtraBig + 4,
                      vertical: AppTheme.paddingVerticalSmall),
                  child: PinCodeTextField(
                    textInputType: TextInputType.phone,
                    length: 6,
                    obsecureText: false,
                    animationType: AnimationType.fade,
                    pinTheme: PinTheme(
                      shape: PinCodeFieldShape.box,
                      borderRadius: AppTheme.borderRadiusSmall,
                      fieldHeight: 50,
                      fieldWidth: 40,
                      activeFillColor: Colors.white,
                      inactiveFillColor: AppTheme.primaryAppColor,
                      selectedFillColor: Colors.greenAccent,
                    ),
                    animationDuration: Duration(milliseconds: 300),
                    backgroundColor: Colors.transparent,
                    enableActiveFill: true,
                    errorAnimationController: errorController,

                    controller: textEditingController,
                    onChanged: (value) {
                      setState(() {
                        pinNo = value;
                      });
                    },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: AppTheme.paddingHorizontalExtraBig + 4),
                  child: Text(
                    hasError
                        ? FlutterI18n.translate(context, "error_msg.cellEmpty")
                        : "",
                    style: TextStyle(
                        color: Colors.red.shade50,
                        fontWeight: FontWeight.bold,
                        fontSize: AppTheme.subtitleFontSize),
                  ),
                ),
                AppTheme.dividerVerticalBig,
                RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                      text:
                          '${FlutterI18n.translate(context, "text_label.notReceiveCode")} ',
                      style: TextStyle(
                          color: Colors.black87,
                          fontSize: AppTheme.bodyFontSize),
                      children: [
                        TextSpan(
                            text:
                                " ${FlutterI18n.translate(context, "btn_label.resend")}",
                            recognizer: onTapRecognizer,
                            style: TextStyle(
                                color: AppTheme.primaryAppColor,
                                fontWeight: FontWeight.bold,
                                fontSize: AppTheme.subtitleFontSize))
                      ]),
                ),
                AppTheme.dividerVerticalSmall,
                Container(
                  margin: EdgeInsets.only(
                    left: AppTheme.paddingHorizontalExtraBig + 4,
                    right: AppTheme.paddingHorizontalExtraBig + 4,
                    top: AppTheme.paddingVerticalExtraSmall,
                  ),
                  child: ButtonTheme(
                    height: 50,
                    child: FlatButton(
                      onPressed: validatePinNumber,
                      child: Center(
                          child: Text(
                        FlutterI18n.translate(context, "btn_label.verify")
                            ?.toUpperCase(),
                        style: TextStyle(
                            letterSpacing: 1.5,
                            color: Colors.white,
                            fontSize: AppTheme.titleFontSize,
                            fontWeight: FontWeight.bold),
                      )),
                    ),
                  ),
                  decoration: BoxDecoration(
                      color: AppTheme.primaryAppColor,
                      borderRadius: AppTheme.borderRadiusSmall,
                      boxShadow: [
                        BoxShadow(
                            color: AppTheme.primaryAppColor.withOpacity(0.3),
                            offset: Offset(1, -2),
                            blurRadius: 5),
                        BoxShadow(
                            color: AppTheme.primaryAppColor.withOpacity(0.3),
                            offset: Offset(-1, 2),
                            blurRadius: 5)
                      ]),
                ),
                Center(
                  child: FlatButton(
                    child: Text(
                      FlutterI18n.translate(context, "btn_label.clear")
                          ?.toUpperCase(),
                      style: TextStyle(
                          color: AppTheme.white, fontWeight: FontWeight.bold),
                    ),
                    onPressed: () {
                      textEditingController.clear();
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> submitPhoneNumber() async {
    Get.focusScope.unfocus();
    try {
      setState(() {
        _isLoading = true;
      });

//      var result = await AuthController.getInstance.requestOtp();
//      if (result != null) {
//        setState(() {
      isShowingSnackBar = true;
//        });
      SnackbarWidget.showCustom(
        title: FlutterI18n.translate(context, "text_label.otpCode"),
        message:
            '${FlutterI18n.translate(context, "info_msg.yourOTPis")} 123456',
        isDismissible: true,
        mainButton: FlatButton(
          highlightColor: Colors.transparent,
          splashColor: Colors.transparent,
          onPressed: () {
            Get.back();
            setState(() {
              isShowingSnackBar = false;
            });
          },
          child: Text(
            FlutterI18n.translate(context, "btn_label.ok")?.toUpperCase(),
            style: TextStyle(color: Colors.white),
          ),
        ),
      );
      await DialogWidget.successDialog(
          body:
              '${FlutterI18n.translate(context, "success_msg.verificationCodeSentSuccess")} ${widget.phoneNumber}');
//      } else {
//        await DialogWidget.errorDialog(
//            body: FlutterI18n.translate(
//                context, "error_msg.verificationCodeSentError"));
//      }
//      }
    } on HttpException catch (error) {
      await DialogWidget.errorDialog(body: error.toString());
    } catch (error) {
      await DialogWidget.errorDialog(
          body: FlutterI18n.translate(
              context, "error_msg.verificationCodeSentError"));
    }

    setState(() {
      _isLoading = false;
    });
  }

  void validatePinNumber() async {
    Get.focusScope.unfocus();
    // conditions for validating
    if (pinNo.length != 6) {
      errorController
          .add(ErrorAnimationType.shake); // Triggering error shake animation
      setState(() {
        hasError = true;
      });
      return;
    }

    try {
      setState(() {
        _isLoading = true;
        hasError = false;
      });
//      var isSuccess = await AuthController.getInstance.submitOtp(pinNo);
//      if (isSuccess != null && isSuccess) {
      if (isShowingSnackBar) {
        Get.back();
        setState(() {
          isShowingSnackBar = false;
        });
      }

      if (Get.isSnackbarOpen) {
        Get.back();
      }

      await DialogWidget.successDialog(
          body: widget.successMessage != null
              ? FlutterI18n.translate(context, widget.successMessage)
              : 'Success');

      Get.back(result: true);
//      } else {
//        await DialogWidget.errorDialog(
//            body: FlutterI18n.translate(context, "error_msg.verifyPhoneError"));
//      }
    } catch (error) {
      await DialogWidget.generalErrorWidget(context: context);
    }

    setState(() {
      _isLoading = false;
    });
  }
}
