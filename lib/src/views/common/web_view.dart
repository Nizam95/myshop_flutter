import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:get/get.dart';
import 'package:myshop_flutter/src/constants/enum_constant.dart';
import 'package:myshop_flutter/src/controllers/web_controller.dart';
import 'package:myshop_flutter/src/widgets/dialog_widget.dart';

class WebPageViewer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<WebController>(
      init: WebController(),
      builder: (c) => WillPopScope(
        onWillPop: () async {
          if (!c.isRedirect.value) {
            c.flutterWebViewPlugin.hide();
            DialogAction result =
                await DialogWidget.leavePageDialogWidget(context: context);
            if (result == DialogAction.yes) {
              await c.flutterWebViewPlugin.close();
              Get.back();
            } else {
              await c.flutterWebViewPlugin.show();
            }
          } else {
            await c.flutterWebViewPlugin.close();
            Get.back(result: true);
          }
          return false;
        },
        child: WebviewScaffold(
          key: c.scaffoldKey,
          url: c.url,
          mediaPlaybackRequiresUserGesture: false,
          javascriptChannels: c.javascriptChannel,
          withJavascript: true,
          appBar: AppBar(
            title: FittedBox(
                fit: BoxFit.fitWidth,
                child: Text(c.title != null ? c.title : '')),
          ),
          withZoom: true,
          withLocalStorage: true,
          hidden: true,
          enableAppScheme: true,
          resizeToAvoidBottomInset: true,
          scrollBar: true,
          supportMultipleWindows: false,
          initialChild: Container(
            child: const Center(
              child: CircularProgressIndicator(),
            ),
          ),
        ),
      ),
    );
  }
}
