import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:get/get.dart';

import 'package:jiffy/jiffy.dart';
import 'package:intl/intl.dart';

import 'package:myshop_flutter/src/constants/enum_constant.dart';
import 'package:myshop_flutter/src/constants/app_theme_constant.dart';
import 'package:myshop_flutter/src/controllers/auth_controller.dart';
import 'package:myshop_flutter/src/utils/utils.dart';
import 'package:myshop_flutter/src/utils/validator.dart';
import 'package:myshop_flutter/src/widgets/button/round_button_widget.dart';
import 'package:myshop_flutter/src/widgets/dialog_widget.dart';
import 'package:myshop_flutter/src/widgets/form/border_text_form_field_widget.dart';
import 'package:myshop_flutter/src/widgets/page/page_container_widget.dart';

class RegisterPage extends StatelessWidget {
  final formKey = GlobalKey<FormState>();
  final AuthController controller = AuthController.getInstance;
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        DialogAction result =
            await DialogWidget.discardDialogWidget(context: context);
        if (result == DialogAction.yes) {
          Get.close(2);
        }
        return true;
      },
      child: PageContainerWidget(
          inAsyncCall: controller.isLoading,
          showBackButton: true,
          formKey: formKey,
          buttonWidget: Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: AppTheme.padding40),
              child: _buildSignUpBtn(context),
            ),
          ),
          onPressedBackButton: () async {
            DialogAction result =
                await DialogWidget.discardDialogWidget(context: context);
            if (result == DialogAction.yes) {
              Get.close(2);
            }
            return true;
          },
          widgetList: <Widget>[
            Text(
              FlutterI18n.translate(context, "btn_label.signUp"),
              style: TextStyle(
                color: Colors.white,
                fontSize: AppTheme.headlineFontSize,
                fontWeight: FontWeight.bold,
              ),
            ),
            AppTheme.dividerVerticalBig,
            _buildUsernameTF(context),
            AppTheme.dividerVerticalSmall,
            _buildDobTF(context),
            AppTheme.dividerVerticalSmall,
            _buildPasswordTF(context),
            AppTheme.dividerVerticalSmall,
            _buildConfirmPasswordTF(context),
            AppTheme.dividerVerticalSmall,
          ]),
    );
  }

  Widget _buildUsernameTF(BuildContext context) {
    return BorderTextFieldWidget(
      controller: controller.usernameController,
      prefixIcon: Icons.person,
      validator: (val) {
        return Validator.validateField(val);
      },
      labelText: FlutterI18n.translate(context, "text_label.username"),
      hintText: FlutterI18n.translate(context, "hint_label.enterUserName"),
    );
  }

  Widget _buildDobTF(BuildContext context) {
    return GestureDetector(
        onTap: () async {
          Get.focusScope.unfocus();
          DateTime initialDate = Jiffy(DateTime.now()).subtract(years: 20);
          DateTime lastDate = Jiffy(DateTime.now()).subtract(years: 16);
          DateTime selectedDate = await Util.selectDate(
              context: context,
              pickerMode: DatePickerMode.year,
              dateState: initialDate,
              lastDate: lastDate);

          if (selectedDate != null) {
            String formattedDate =
                DateFormat('yyyy-MM-dd').format(selectedDate);
            controller.dobController.text = formattedDate;
          }
        },
        child: Container(
          color: Colors.transparent,
          child: IgnorePointer(
            child: BorderTextFieldWidget(
              controller: controller.dobController,
              readonly: true,
              prefixIcon: Icons.cake,
              validator: (val) {
                return Validator.validateField(val);
              },
              labelText: FlutterI18n.translate(context, "text_label.dob"),
              hintText: FlutterI18n.translate(context, "hint_label.enterDob"),
            ),
          ),
        ));
  }

  Widget _buildPasswordTF(BuildContext context) {
    return BorderTextFieldWidget(
      controller: controller.passwordController,
      obscureText: controller.obscurePasswordText.value,
      prefixIcon: Icons.lock,
      validator: (val) {
        return Validator.validatePasswordField(val);
      },
      suffixIcon: GestureDetector(
        onTap: () {
          Get.focusScope.unfocus();
          controller.obscurePasswordText.value =
              !controller.obscurePasswordText.value;
        },
        child: Icon(
          controller.obscurePasswordText.value
              ? Icons.visibility_off
              : Icons.visibility,
          color: Colors.black54,
        ),
      ),
      labelText: FlutterI18n.translate(context, "text_label.password"),
      hintText: FlutterI18n.translate(context, "hint_label.enterPassword"),
    );
  }

  Widget _buildConfirmPasswordTF(BuildContext context) {
    return BorderTextFieldWidget(
      controller: controller.confirmPasswordController,
      prefixIcon: Icons.lock_outline,
      obscureText: controller.obscureConfirmPasswordText.value,
      validator: (val) {
        return Validator.validateConfirmPasswordField(
            controller.passwordController.text, val);
      },
      suffixIcon: GestureDetector(
        onTap: () {
          Get.focusScope.unfocus();
          controller.obscureConfirmPasswordText.value =
              !controller.obscureConfirmPasswordText.value;
        },
        child: Icon(
          controller.obscureConfirmPasswordText.value
              ? Icons.visibility_off
              : Icons.visibility,
          color: Colors.black54,
        ),
      ),
      labelText: FlutterI18n.translate(context, "text_label.confirmPassword"),
      hintText: FlutterI18n.translate(context, "hint_label.enterPasswordAgain"),
    );
  }

  Widget _buildSignUpBtn(BuildContext context) {
    return RoundButtonWidget(
      callback: () async {
        Get.focusScope.unfocus();
        try {
//          if (!formKey.currentState.validate()) {
//            await DialogWidget.fieldErrorWidget(context: context);
//            return;
//          } else {
//          var result = await controller.register();
//          if (result != null && result == true) {
          await DialogWidget.successDialog(
              body: FlutterI18n.translate(
                  context, "success_msg.registrationSuccess"));
          Get.close(2);
//            }
//          }
        } catch (error) {
          await DialogWidget.generalErrorWidget(context: context);
          return;
        }
      },
      btnText:
          FlutterI18n.translate(context, "btn_label.register")?.toUpperCase(),
    );
  }
}
