import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:get/get.dart';
import 'package:myshop_flutter/src/constants/app_constant.dart';
import 'package:myshop_flutter/src/constants/assets_path.dart';
import 'package:myshop_flutter/src/constants/enum_constant.dart';
import 'package:myshop_flutter/src/constants/app_theme_constant.dart';
import 'package:myshop_flutter/src/constants/messages_constant.dart';
import 'package:myshop_flutter/src/controllers/auth_controller.dart';
import 'package:myshop_flutter/src/utils/validator.dart';
import 'package:myshop_flutter/src/widgets/button/round_button_widget.dart';
import 'package:myshop_flutter/src/widgets/dialog_widget.dart';
import 'package:myshop_flutter/src/widgets/form/text_field_widget.dart';
import 'package:myshop_flutter/src/widgets/header_widget.dart';

class ChangePasswordPage extends GetView<AuthController> {
  final formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Get.focusScope.unfocus(),
      child: Scaffold(
        appBar: AppTheme.defaultAppBar,
        body: SafeArea(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              AppTheme.dividerVerticalBig,
              HeaderWidget(
                iconImage: AssetsPath.Lock,
                title: AppConstant.ChangePassword.toUpperCase(),
              ),
              Expanded(flex: 5, child: buildBody(context)),
              Flexible(flex: 2, child: buildSavedBtn(context))
            ],
          ),
        ),
      ),
    );
  }

  Widget buildBody(BuildContext context) {
    return Container(
      padding:
          EdgeInsets.symmetric(horizontal: AppTheme.paddingHorizontalMediumBig),
      child: SingleChildScrollView(
        child: Form(
          key: formKey,
          child: Column(
            children: <Widget>[
              Obx(
                () => TextFieldWidget(
                  label: FlutterI18n.translate(
                          context, "text_label.currentPassword")
                      ?.toUpperCase(),
                  isSecuredText: controller.obscurePasswordText.value,
                  suffixOnTap: () => controller.obscurePasswordText.value =
                      !controller.obscurePasswordText.value,
                  controller: controller.newPasswordController,
                  suffixVisibility: true,
                  suffixType: SuffixType.visibility,
                  validator: (val) {
                    return Validator.validatePasswordField(val);
                  },
                  textInputType: TextInputType.text,
                  hint: FlutterI18n.translate(
                          context, "hint_label.enterCurrentPassword")
                      ?.toUpperCase(),
                ),
              ),
              Obx(
                () => TextFieldWidget(
                  label:
                      FlutterI18n.translate(context, "text_label.newPassword")
                          ?.toUpperCase(),
                  isSecuredText: controller.obscureNewPasswordText.value,
                  suffixOnTap: () => controller.obscureNewPasswordText.value =
                      !controller.obscureNewPasswordText.value,
                  suffixVisibility: true,
                  suffixType: SuffixType.visibility,
                  controller: controller.newPasswordController,
                  validator: (val) {
                    return Validator.validatePasswordField(val);
                  },
                  textInputType: TextInputType.text,
                  hint: FlutterI18n.translate(
                          context, "hint_label.enterNewPassword")
                      ?.toUpperCase(),
                ),
              ),
              Obx(() => TextFieldWidget(
                    label: FlutterI18n.translate(
                            context, "text_label.confirmNewPassword")
                        ?.toUpperCase(),
                    isSecuredText: controller.obscureConfirmPasswordText.value,
                    suffixOnTap: () => controller.obscureConfirmPasswordText
                        .value = !controller.obscureConfirmPasswordText.value,
                    suffixVisibility: true,
                    suffixType: SuffixType.visibility,
                    controller: controller.newPasswordController,
                    validator: (val) {
                      return Validator.validateConfirmPasswordField(
                          controller.newPasswordController.text, val);
                    },
                    textInputType: TextInputType.text,
                    hint: FlutterI18n.translate(
                            context, "hint_label.enterNewPasswordAgain")
                        ?.toUpperCase(),
                  )),
            ],
          ),
        ),
      ),
    );
  }

  ///TODO: MAKE API CONNECTION
  Widget buildSavedBtn(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: AppTheme.padding40),
      child: RoundButtonWidget(
          callback: () async {
            Get.focusScope.unfocus();
            try {
              if (!formKey.currentState.validate()) {
                await DialogWidget.fieldErrorWidget(context: context);
                return;
              }

              await DialogWidget.successDialog(
                  body: FlutterI18n.translate(
                      context, "success_msg.changePasswordSuccess"));
            } catch (error) {
              print(error.toString());
              await DialogWidget.errorDialog(body: ErrorMessage.generalError);
              return;
            }
          },
          btnText: FlutterI18n.translate(context, "btn_label.proceed")
              ?.toUpperCase()),
    );
  }
}
