import 'package:flutter/material.dart';
import 'package:myshop_flutter/src/constants/app_constant.dart';
import 'package:myshop_flutter/src/constants/app_theme_constant.dart';
import 'package:myshop_flutter/src/constants/assets_path.dart';
import 'package:myshop_flutter/src/widgets/header_widget.dart';

class NotificationPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppTheme.defaultAppBar,
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
//            AppTheme.dividerVerticalBig,
//            HeaderWidget(
//              iconImage: AssetsPath.Notification,
//              title: AppConstant.Notification.toUpperCase(),
//            ),
            Expanded(
              child: Center(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.notifications_off,
                    color: AppTheme.primaryAppColor,
                    size: 72,
                  ),
                  AppTheme.dividerVerticalBig,
                  Flexible(
                    child: Text(
                      "YOU HAVE NO NOTIFICATION",
                      style: TextStyle(
                          fontSize: AppTheme.bigTitleFontSize,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                ],
              )),
            ),

//            Expanded(
//              child: ListView.builder(
//                itemCount: 20,
//                itemBuilder: (context, index) {
//                  return NotificationBodyWidget();
//                },
//              ),
//            ),
          ],
        ),
      ),
    );
  }
}
