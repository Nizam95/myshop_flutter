import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:myshop_flutter/src/constants/app_constant.dart';
import 'package:myshop_flutter/src/constants/app_theme_constant.dart';
import 'package:myshop_flutter/src/constants/assets_path.dart';
import 'package:myshop_flutter/src/controllers/auth_controller.dart';
import 'package:myshop_flutter/src/widgets/header_widget.dart';

class ProfilePage extends StatelessWidget {
  final AuthController controller = AuthController.getInstance;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppTheme.defaultAppBar,
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            AppTheme.dividerVerticalBig,
            HeaderWidget(
              iconImage: AssetsPath.Person,
              title: AppConstant.Profile.toUpperCase(),
            ),
            Expanded(
                child: ListView(
              children: <Widget>[
                buildListTile(
                    title: FlutterI18n.translate(context, "text_label.name")
                        ?.toUpperCase(),
                    subtitle: controller.user.value.fullName != null
                        ? controller.user.value.fullName.toUpperCase()
                        : ''),
                buildListTile(
                    title:
                        FlutterI18n.translate(context, "text_label.phoneNumber")
                            ?.toUpperCase(),
                    subtitle: controller.user.value.phoneNumber != null
                        ? controller.user.value.phoneNumber
                        : ''),
                buildListTile(
                    title: FlutterI18n.translate(context, "text_label.dob")
                        ?.toUpperCase(),
                    subtitle: controller.user.value.birthDate != null
                        ? controller.user.value.birthDate
                        : ''),
              ],
            )),
          ],
        ),
      ),
    );
  }

  Widget buildListTile({String title, String subtitle}) {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: AppTheme.paddingHorizontalExtraBig),
      child: ListTile(
          title: Text(
            title,
            style: TextStyle(
              fontSize: AppTheme.bodyFontSize,
              color: AppTheme.grey,
            ),
          ),
          subtitle: Text(
            subtitle,
            style: TextStyle(
                fontSize: AppTheme.smallTitleFontSize,
                color: AppTheme.textPrimaryColor),
          )),
    );
  }
}
