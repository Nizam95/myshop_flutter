import 'package:flutter/material.dart';
import 'package:myshop_flutter/src/constants/app_theme_constant.dart';
import 'package:myshop_flutter/src/constants/assets_path.dart';

class SplashScreenPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              AssetsPath.AppLogo,
              scale: 3.0,
            ),
            AppTheme.dividerVerticalExtraBig,
            CircularProgressIndicator()
          ],
        ),
      )),
    );
  }
}
