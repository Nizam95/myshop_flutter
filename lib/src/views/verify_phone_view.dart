import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:get/get.dart';
import 'package:myshop_flutter/src/constants/app_theme_constant.dart';
import 'package:myshop_flutter/src/constants/route_path.dart';
import 'package:myshop_flutter/src/controllers/auth_controller.dart';
import 'package:myshop_flutter/src/utils/validator.dart';
import 'package:myshop_flutter/src/views/common/pin_code_view.dart';
import 'package:myshop_flutter/src/widgets/button/round_button_widget.dart';
import 'package:myshop_flutter/src/widgets/dialog_widget.dart';
import 'package:myshop_flutter/src/widgets/form/border_text_form_field_widget.dart';
import 'package:myshop_flutter/src/widgets/page/page_container_widget.dart';
import 'package:myshop_flutter/src/widgets/snackbar_widget.dart';

class VerifyPhonePage extends StatelessWidget {
  final formKey = GlobalKey<FormState>();
  final AuthController controller = AuthController.getInstance;
  @override
  Widget build(BuildContext context) {
    return PageContainerWidget(
        inAsyncCall: controller.isLoading,
        showBackButton: true,
        formKey: formKey,
        buttonWidget: Align(
          alignment: Alignment.bottomCenter,
          child: Padding(
            padding: EdgeInsets.only(
                left: AppTheme.padding40,
                right: AppTheme.padding40,
                bottom: AppTheme.padding40),
            child: _buildLoginBtn(context),
          ),
        ),
        onPressedBackButton: () {
          Get.focusScope.unfocus();
          Get.back();
        },
        widgetList: <Widget>[
          Text(
            FlutterI18n.translate(context, "text_label.verifyPhoneFirst"),
            style: TextStyle(
              color: Colors.white,
              fontSize: AppTheme.headlineFontSize,
              fontWeight: FontWeight.bold,
            ),
          ),
          AppTheme.dividerVerticalBig,
          _buildPhoneTF(context),
          AppTheme.dividerVerticalBig,
        ]);
  }

  Widget _buildPhoneTF(BuildContext context) {
    return BorderTextFieldWidget(
      controller: controller.phoneNumberController,
      prefixIcon: Icons.phone_android,
      prefixText: "+60",
      labelText: FlutterI18n.translate(context, "text_label.phoneNumber"),
      hintText: FlutterI18n.translate(context, "hint_label.enterPhoneNumber"),
      validator: (val) {
        return Validator.validatePhoneField(val);
      },
      textInputType: TextInputType.phone,
      maxLength: 14,
    );
  }

  Widget _buildLoginBtn(BuildContext context) {
    return RoundButtonWidget(
      callback: () async {
        Get.focusScope.unfocus();
        try {
//          if (!formKey.currentState.validate()) {
//            await DialogWidget.fieldErrorWidget(context: context);
//            return;
//          } else {
//          var result = await controller.requestOtp();
//          if (result != null) {
          Get.to(
                  PinCodePage(
                      successMessage: "success_msg.verifyPhoneSuccess",
                      phoneNumber:
                          '60${controller.phoneNumberController.text}'),
                  transition: Transition.topLevel)
              .then((value) => value != null && value == true
                  ? Get.toNamed(RoutePaths.RegisterPage)
                  : null);
          SnackbarWidget.showCustom(
            title: FlutterI18n.translate(context, "text_label.otpCode"),
            message:
                '${FlutterI18n.translate(context, "info_msg.yourOTPis")} 123456',
            isDismissible: true,
            instantInit: false,
            mainButton: FlatButton(
              highlightColor: Colors.transparent,
              splashColor: Colors.transparent,
              onPressed: () {
                if (Get.isSnackbarOpen) {
                  Get.back();
                }
              },
              child: Text(
                FlutterI18n.translate(context, "btn_label.ok")?.toUpperCase(),
                style: TextStyle(color: Colors.white),
              ),
            ),
          );
//          } else {
//            SnackbarWidget.showError(ErrorMessage.generalError);
//            return;
//          }
//          }
        } catch (e) {
          await DialogWidget.generalErrorWidget(context: context);
          return;
        }
      },
      btnText:
          FlutterI18n.translate(context, "btn_label.verify")?.toUpperCase(),
    );
  }
}
