import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:get/get.dart';
import 'package:myshop_flutter/src/constants/route_path.dart';
import 'package:myshop_flutter/src/constants/app_theme_constant.dart';
import 'package:myshop_flutter/src/constants/messages_constant.dart';
import 'package:myshop_flutter/src/controllers/auth_controller.dart';
import 'package:myshop_flutter/src/utils/validator.dart';
import 'package:myshop_flutter/src/widgets/button/round_button_widget.dart';
import 'package:myshop_flutter/src/widgets/dialog_widget.dart';
import 'package:myshop_flutter/src/widgets/form/border_text_form_field_widget.dart';
import 'package:myshop_flutter/src/widgets/page/page_container_widget.dart';

class LoginPage extends StatelessWidget {
  final formKey = GlobalKey<FormState>();
  final AuthController controller = AuthController.getInstance;
  @override
  Widget build(BuildContext context) {
    return PageContainerWidget(
      inAsyncCall: controller.isLoading,
      formKey: formKey,
      widgetList: <Widget>[
        Text(
          FlutterI18n.translate(context, "title.signIn"),
          style: TextStyle(
            color: Colors.white,
            fontSize: AppTheme.headlineFontSize,
            fontWeight: FontWeight.bold,
          ),
        ),
        AppTheme.dividerVerticalBig,
        _buildUsernameTF(context),
        AppTheme.dividerVerticalBig,
        _buildPasswordTF(context),
        _buildForgotPasswordBtn(context),
        AppTheme.dividerVerticalBig,
      ],
      buttonWidget: Padding(
        padding: EdgeInsets.only(
            left: AppTheme.padding40,
            right: AppTheme.padding40,
            bottom: AppTheme.padding40),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            _buildLoginBtn(context),
            _buildSignUpBtn(context),
          ],
        ),
      ),
    );
  }

  Widget _buildUsernameTF(BuildContext context) {
    return BorderTextFieldWidget(
      controller: controller.usernameController,
      validator: (val) {
        return Validator.validateField(val);
      },
      prefixIcon: Icons.person,
      labelText: FlutterI18n.translate(context, "text_label.username"),
      hintText: FlutterI18n.translate(context, "hint_label.enterUserName"),
    );
  }

  Widget _buildPasswordTF(BuildContext context) {
    return BorderTextFieldWidget(
      controller: controller.passwordController,
      obscureText: controller.obscurePasswordText.value,
      prefixIcon: Icons.lock,
      validator: (val) {
        return Validator.validatePasswordField(val);
      },
      suffixIcon: GestureDetector(
        onTap: () {
          Get.focusScope.unfocus();
          controller.obscurePasswordText.value =
              !controller.obscurePasswordText.value;
        },
        child: Icon(
          controller.obscurePasswordText.value
              ? Icons.visibility_off
              : Icons.visibility,
          color: Colors.black54,
        ),
      ),
      labelText: FlutterI18n.translate(context, "text_label.password"),
      hintText: FlutterI18n.translate(context, "hint_label.enterPassword"),
    );
  }

  Widget _buildForgotPasswordBtn(BuildContext context) {
    return Container(
      alignment: Alignment.centerRight,
      child: FlatButton(
        onPressed: () {},
        padding: EdgeInsets.only(right: 0.0),
        child: Text(
          FlutterI18n.translate(context, "btn_label.forgotPassword"),
          style: AppTheme.kLabelStyle,
        ),
      ),
    );
  }

  Widget _buildLoginBtn(BuildContext context) {
    return RoundButtonWidget(
      callback: () async {
        Get.focusScope.unfocus();
        try {
//          Get.toNamed(RoutePaths.MainPage);
//          if (!formKey.currentState.validate()) {
//            await DialogWidget.fieldErrorWidget(context: context);
//            return;
//          } else {
          await controller.login();
//          }
        } catch (error) {
          await DialogWidget.errorDialog(body: ErrorMessage.generalError);
          return;
        }
      },
      btnText: FlutterI18n.translate(context, "btn_label.login")?.toUpperCase(),
    );
  }

  Widget _buildSignUpBtn(BuildContext context) {
    return GestureDetector(
      onTap: () {
        controller.clearTextField();
        Get.toNamed(RoutePaths.VerifyPhonePage);
      },
      child: RichText(
        text: TextSpan(
          children: [
            TextSpan(
              text:
                  '${FlutterI18n.translate(context, "text_label.haveAccount")} ',
              style: TextStyle(
                color: Colors.white,
                fontSize: AppTheme.subtitleFontSize,
                fontWeight: FontWeight.w400,
              ),
            ),
            TextSpan(
              text: FlutterI18n.translate(context, "btn_label.signUp"),
              style: TextStyle(
                color: Colors.white,
                fontSize: AppTheme.titleFontSize,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
