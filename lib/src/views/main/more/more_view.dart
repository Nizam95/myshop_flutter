import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:get/get.dart';
import 'package:myshop_flutter/src/constants/assets_path.dart';

class MorePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Image.asset(
            AssetsPath.AppLogo,
            scale: 3.0,
          ),
          SizedBox(
            height: 16,
          ),
          Text(
            FlutterI18n.translate(context, "title.comingSoon")?.toUpperCase(),
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: Get.theme.textTheme.headline5.fontSize,
                fontWeight: FontWeight.bold,
                color: Get.theme.primaryColor),
          ),
        ],
      ),
    ));
  }
}
