import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:myshop_flutter/src/constants/app_theme_constant.dart';

class ActivityBodyWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: AppTheme.elevationBig,
      shape: AppTheme.roundBorderSmall,
      margin: EdgeInsets.symmetric(
          horizontal: AppTheme.marginHorizontalBig,
          vertical: AppTheme.marginVerticalSmall),
      child: Container(
          padding: AppTheme.paddingBig,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'BILL PAYMENT',
                style: TextStyle(
                  fontSize: AppTheme.bodyFontSize,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                DateFormat('dd/MM/yyyy hh:mm a').format(DateTime.now()),
                style: TextStyle(
                  fontSize: AppTheme.bodyFontSize,
                ),
              ),
              AppTheme.dividerVerticalSmall,
              Text(
                'Made bill payment (Syarikat Air Selangor)',
                style: TextStyle(
                  fontSize: AppTheme.bodyFontSize,
                ),
              ),
            ],
          )),
    );
  }
}
