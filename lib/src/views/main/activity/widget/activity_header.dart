import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myshop_flutter/src/constants/app_theme_constant.dart';

class ActivityHeaderWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      padding: AppTheme.paddingBig,
      color: AppTheme.primaryAppColor.withOpacity(0.7),
      child: Text(
        'RECENT ACTIVITY',
        textAlign: TextAlign.center,
        style: TextStyle(
            fontSize: AppTheme.titleFontSize,
            fontWeight: FontWeight.bold,
            color: AppTheme.white),
      ),
    );
  }
}
