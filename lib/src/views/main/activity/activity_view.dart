import 'package:flutter/material.dart';
import 'package:myshop_flutter/src/views/main/activity/widget/activity_body.dart';
import 'package:myshop_flutter/src/views/main/activity/widget/activity_header.dart';

class ActivityPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Flexible(
          child: ActivityHeaderWidget(),
        ),
        Expanded(
          flex: 8,
          child: ListView.builder(
            itemCount: 20,
            itemBuilder: (context, index) {
              return ActivityBodyWidget();
            },
          ),
        )
      ],
    ));
  }
}
