import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myshop_flutter/src/constants/app_theme_constant.dart';
import 'package:myshop_flutter/src/constants/assets_path.dart';
import 'package:myshop_flutter/src/constants/route_path.dart';
import 'package:myshop_flutter/src/models/marker_model.dart';
import 'package:myshop_flutter/src/widgets/button/square_button_widget.dart';

class BottomSheetWidget extends StatelessWidget {
  final MarkerModel data;

  const BottomSheetWidget({Key key, this.data}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          SizedBox(
            width: Get.width,
            child: SquareButtonWidget(
              padding: AppTheme.paddingBig,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(2)),
              btnText: "VIEW SHOP",
              textColor: Colors.white,
              callback: () {
                Get.toNamed(RoutePaths.WebPageViewer,
                    arguments: {"url": data.websiteUrl, "title": data.name});
              },
              btnColor: AppTheme.primaryColor,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
              width: Get.width,
              padding: AppTheme.paddingSmall,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    AssetsPath.ShopIcon,
                    width: 24,
                    height: 24,
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Expanded(
                      child: Padding(
                    padding: EdgeInsets.only(
                        right: AppTheme.paddingHorizontalMedium),
                    child: Text(
                      data.name,
                      style: TextStyle(
                          fontSize: AppTheme.titleFontSize,
                          fontWeight: FontWeight.bold),
                      textAlign: TextAlign.start,
                    ),
                  )),
                ],
              )),
          Divider(
            thickness: 2,
          ),
          Container(
            child: Row(
              children: <Widget>[
                Flexible(
                    flex: 1,
                    child: Padding(
                      padding: AppTheme.paddingSmall,
                      child: Icon(
                        Icons.phone,
                        color: AppTheme.accentAppColor,
                      ),
                    )),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  flex: 9,
                  child: Text(
                    data.phoneNumber,
                    style: TextStyle(fontSize: AppTheme.subtitleFontSize),
                  ),
                )
              ],
            ),
          ),
          Divider(
            thickness: 2,
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Flexible(
                  child: Padding(
                padding: AppTheme.paddingSmall,
                child: Icon(
                  Icons.map,
                  color: AppTheme.accentAppColor,
                ),
              )),
              SizedBox(
                width: 10,
              ),
              Expanded(
                flex: 9,
                child: Padding(
                  padding: EdgeInsets.only(
                      right: AppTheme.paddingHorizontalMediumBig),
                  child: Text(
                    data.address,
                    style: TextStyle(fontSize: AppTheme.subtitleFontSize),
                    softWrap: true,
                  ),
                ),
              )
            ],
          ),
          SizedBox(
            height: 16,
          ),
        ],
      ),
    );
  }
}
