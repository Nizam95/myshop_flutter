import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:implicitly_animated_reorderable_list/implicitly_animated_reorderable_list.dart';
import 'package:implicitly_animated_reorderable_list/transitions.dart';
import 'package:material_floating_search_bar/material_floating_search_bar.dart';
import 'package:myshop_flutter/src/constants/app_data.dart';
import 'package:myshop_flutter/src/constants/app_theme_constant.dart';
import 'package:myshop_flutter/src/controllers/main_controller.dart';
import 'package:myshop_flutter/src/models/place_model.dart';

class FloatingSearchBarWidget extends GetView<MainController> {
  @override
  Widget build(BuildContext context) {
    return Obx(
      () => FloatingSearchBar(
        hint: 'Search...',
        backgroundColor: Colors.white,
        clearQueryOnClose: true,
        scrollPadding: const EdgeInsets.only(top: 16, bottom: 56),
        transitionDuration: const Duration(milliseconds: 800),
        transitionCurve: Curves.easeInOut,
        physics: const BouncingScrollPhysics(),
        axisAlignment: 0.0,
        openAxisAlignment: 0.0,
        maxWidth: Get.width - 50,
        debounceDelay: const Duration(milliseconds: 500),
        onQueryChanged: controller.onQueryChanged,
        // Specify a custom transition to be used for
        // animating between opened and closed stated.
        transition: CircularFloatingSearchBarTransition(),
        actions: [
          FloatingSearchBarAction(
            showIfOpened: false,
            child: CircularButton(
              icon: const Icon(Icons.place),
              onPressed: () {},
            ),
          ),
          FloatingSearchBarAction.searchToClear(
            showIfClosed: false,
          ),
        ],
        progress: controller.isLoading.value,
        builder: (context, transition) {
          return Material(
            color: Colors.white,
            elevation: 4.0,
            borderRadius: BorderRadius.circular(8),
            child: ImplicitlyAnimatedList<PlaceModel>(
              shrinkWrap: true,
              items: controller.suggestions.value.take(6).toList(),
              physics: const NeverScrollableScrollPhysics(),
              areItemsTheSame: (a, b) => a == b,
              itemBuilder: (context, animation, place, i) {
                return SizeFadeTransition(
                  animation: animation,
                  child: buildItem(context, place),
                );
              },
              updateItemBuilder: (context, animation, place) {
                return FadeTransition(
                  opacity: animation,
                  child: buildItem(context, place),
                );
              },
            ),
          );
        },
      ),
    );
  }

  Widget buildItem(BuildContext context, PlaceModel place) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        InkWell(
          onTap: () {
            FloatingSearchBar.of(context).close();
            controller.updateMapLocation(place: place);
            Future.delayed(
              const Duration(milliseconds: 500),
              () => controller.clearSearchQuery(),
            );
          },
          child: Padding(
            padding: const EdgeInsets.all(16),
            child: Row(
              children: [
                SizedBox(
                  width: 36,
                  child: AnimatedSwitcher(
                    duration: const Duration(milliseconds: 500),
                    child:
                        controller.suggestions.value == StaticListData.history
                            ? const Icon(Icons.history, key: Key('history'))
                            : const Icon(Icons.place, key: Key('place')),
                  ),
                ),
                const SizedBox(width: 16),
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        place.name,
                        style: TextStyle(fontSize: AppTheme.smallTitleFontSize),
                      ),
                      const SizedBox(height: 2),
                      Text(
                        place.level2Address,
                        style: textTheme.bodyText2
                            .copyWith(color: Colors.grey.shade600),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        if (controller.suggestions.value.isNotEmpty &&
            place != controller.suggestions.value.last)
          const Divider(height: 0),
      ],
    );
  }
}
