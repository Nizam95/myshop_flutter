import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:myshop_flutter/src/controllers/main_controller.dart';
import 'package:myshop_flutter/src/constants/app_theme_constant.dart';
import 'package:myshop_flutter/src/views/main/home/widget/floating_search_bar_widget.dart';

class HomePage extends GetView<MainController> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Obx(() => GoogleMap(
              padding: AppTheme.paddingBig,
              myLocationEnabled: false,
              mapToolbarEnabled: false,
              trafficEnabled: false,
              mapType: MapType.normal,
              onTap: (LatLng position) {
                print(position.toString());
              },
              markers: controller.mapMarkers,
              initialCameraPosition: controller.isLocationChanged.value
                  ? controller.defaultCameraPosition
                  : controller.defaultCameraPosition,
              onMapCreated: (GoogleMapController googleController) {
                controller.mapController = googleController;
              },
            )),
        Align(
          alignment: Alignment.topRight,
          child: Padding(
            padding: EdgeInsets.only(
              top: 60,
              right: AppTheme.paddingHorizontalExtraBig,
            ),
            child: FloatingActionButton(
              mini: true,
              backgroundColor: AppTheme.accentAppColor,
              child: Icon(Icons.gps_fixed),
              onPressed: () async {
                controller.setMapCameraPosition(controller.defaultLatLng);
              },
            ),
          ),
        ),
        FloatingSearchBarWidget()
      ],
    );
  }
}
