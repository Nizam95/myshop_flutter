import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:get/get.dart';

class WebController extends GetxController {
  static WebController getInstance = Get.find();

  @override
  void onInit() {
    super.onInit();
    initArgument();
    initWebController();
  }

  @override
  void onClose() {
    onDestroy.cancel();
    onUrlChanged.cancel();
    onStateChanged.cancel();
    flutterWebViewPlugin.dispose();
    super.onClose();
  }

  ///VARIABLE DECLARATION & INITIALIZATION
  final flutterWebViewPlugin = FlutterWebviewPlugin();
  final scaffoldKey = GlobalKey<ScaffoldState>();
  // On destroy stream
  StreamSubscription onDestroy;

  // On urlChanged stream
  StreamSubscription<String> onUrlChanged;

  // On urlChanged stream
  StreamSubscription<WebViewStateChanged> onStateChanged;
  RxBool isRedirect = false.obs;
  RxString currentUrl = ''.obs;
  Set<JavascriptChannel> javascriptChannel;
  String url;
  String title;

  ///GETTER

  ///SETTER

  ///PERFORM OTHER FUNCTION
  void initWebController() {
    javascriptChannel = [
      JavascriptChannel(
          name: 'Exit',
          onMessageReceived: (JavascriptMessage message) async {
            isRedirect.value = true;
            await flutterWebViewPlugin.close();
            Get.back();
          })
    ].toSet();
    onDestroy = flutterWebViewPlugin.onDestroy.listen((_) {});
    // Add a listener to on url changed
    onUrlChanged = flutterWebViewPlugin.onUrlChanged.listen((String url) {
      currentUrl.value = url;
    });
    onStateChanged =
        flutterWebViewPlugin.onStateChanged.listen((WebViewStateChanged state) {
      if (state.type == WebViewState.finishLoad) {}
    });
  }

  void initArgument() {
    //get data sent from previous route
    Map<String, dynamic> args = Get.arguments as Map<String, dynamic>;
    url = args["url"];
    title = args["title"];
  }
}
