import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myshop_flutter/src/constants/app_constant.dart';
import 'package:myshop_flutter/src/constants/app_data.dart';

import 'package:get_storage/get_storage.dart';

class LocalizationController extends GetxController {
  static LocalizationController getInstance = Get.find();
  final language = "".obs;
  final store = GetStorage();

  String get currentLanguage => language.value;
  Locale get getLocale {
    if (language.value == '' || language.value == null) {
      if ((currentLanguageStore.value == '') ||
          (currentLanguageStore.value == null)) {
        updateLanguage(DefaultData.defaultLanguage);
        return Locale(language.value);
      } else {
        updateLanguage(currentLanguageStore.value);
        return Locale(currentLanguageStore.value);
      }
    } else {
      return Locale(language.value);
    }
  }

  @override
  void onReady() async {
    setInitialLocalLanguage();
    super.onReady();
  }

  // Retrieves and Sets language
  setInitialLocalLanguage() {
    if ((currentLanguageStore.value == '') ||
        (currentLanguageStore.value == null)) {
      updateLanguage(DefaultData.defaultLanguage);
    } else {
      updateLanguage(currentLanguageStore.value);
    }
  }

  // Gets current language stored
  RxString get currentLanguageStore {
    language.value = store.read(StorageKey.LanguageData);
    return language;
  }

// updates the language stored
  Future<void> updateLanguage(String value) async {
    language.value = value;
    await store.write(StorageKey.LanguageData, value);
    Get.updateLocale(getLocale);
    update();
  }
}
