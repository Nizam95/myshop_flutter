import 'dart:collection';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:myshop_flutter/src/constants/app_data.dart';
import 'package:myshop_flutter/src/constants/assets_path.dart';
import 'package:myshop_flutter/src/models/place_model.dart';
import 'package:myshop_flutter/src/services/api_service.dart';
import 'package:myshop_flutter/src/views/main/home/home_view.dart';
import 'package:myshop_flutter/src/views/main/home/widget/bottom_sheet_widget.dart';
import 'package:myshop_flutter/src/views/main/more/more_view.dart';
import 'package:myshop_flutter/src/widgets/dialog_widget.dart';
import 'package:myshop_flutter/src/widgets/navigation/bottom_navigation_widget.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class MainController extends GetxController {
  static MainController getInstance = Get.find();

  ///METHOD OVERRIDING
  @override
  void onReady() async {
    checkLocationPermission();
    await setMarkerIcon();
    addMarker(defaultLatLng);
    //to ensure context properly initialize and avoid call named route null
    //not sure why onReady not initializing instance correctly using schedulerBinding
    //need to explicitly call schedulerBinding
    SchedulerBinding.instance?.addPostFrameCallback((_) {
      listenPosition();
    });
    super.onReady();
  }

  ///VARIABLE DECLARATION & INITIALIZATION
  RxInt selectedBottomNavigationIndex = 0.obs;
  PersistentTabController persistentTabController =
      PersistentTabController(initialIndex: 0);

  RxBool isLoading = false.obs;
  RxList<PlaceModel> suggestions = <PlaceModel>[].obs;
  RxString query = ''.obs;
  Rx<PlaceModel> selectedPlace = PlaceModel(name: '', country: '').obs;

  List<BottomNavigationBarItem> bottomNavigationItems =
      BottomNavigationWidget().getBottomNavigationBarItems();
  List<PersistentBottomNavBarItem> persistentBottomNavigationItems =
      BottomNavigationWidget().getPersistentBottomNavigationBarItems();

  List<Widget> persistentBottomPageList = [
    HomePage(),
    MorePage(),
  ];

  PermissionStatus locationStatus;
  double defaultZoom = 18;
  Geolocator geolocator = Geolocator();
  Rx<Position> position = Position().obs;
  LatLng defaultLatLng = LatLng(3.133243, 101.687054);

  var locationOptions =
      LocationOptions(accuracy: LocationAccuracy.high, distanceFilter: 100);
  RxBool isLocationChanged = false.obs;
  GoogleMapController mapController;
  Set<Marker> mapMarkers = HashSet<Marker>();
  CameraPosition defaultCameraPosition = CameraPosition(
    target: LatLng(3.133243, 101.687054),
    zoom: 18,
  );
  BitmapDescriptor markerIcon;

  ///GETTER

  ///SETTER
  setLoading(bool val) => isLoading.value = val;
  setMapCameraPosition(LatLng latLng) =>
      mapController.animateCamera(CameraUpdate.newCameraPosition(
          CameraPosition(target: latLng, zoom: defaultZoom)));

  setSelectedBottomNavigation(int index) =>
      selectedBottomNavigationIndex.value = index;

  Future<void> setMarkerIcon() async {
    markerIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(), AssetsPath.ShopIcon);
  }

  void onQueryChanged(String qr) async {
    if (qr == query.value) return;

    query.value = qr;
    setLoading(true);

    if (qr.isEmpty) {
      suggestions.value = StaticListData.history;
    } else {
      try {
        var result = await ApiService().getPlaces(parameters: qr);
        if (result != null) {
          var data = result as List;

          suggestions.value =
              data.map((e) => PlaceModel.fromJson(e)).toSet().toList();

          Get.focusScope.unfocus();
        }
      } catch (e) {
        await DialogWidget.errorDialog(body: e.toString());
      }
    }

    setLoading(false);
  }

  ///PERFORM OTHER FUNCTION
  void checkLocationPermission() async {
    locationStatus = await Permission.location.status;

    if (!locationStatus.isGranted) {
      await Permission.location.request();
    }
  }

  void updateMapLocation({PlaceModel place}) async {
    selectedPlace.value = place;
    try {
      print(place.geometry.toJson());
      setMapCameraPosition(LatLng(double.parse(place.geometry.latitude),
          double.parse(place.geometry.longitude)));
    } catch (e) {
      await DialogWidget.errorDialog(body: 'Unable to locate the place');
      setMapCameraPosition(defaultLatLng);
    }
  }

  void addMarker(LatLng ps) {
    StaticListData.markerList.forEach((el) {
      mapMarkers.add(Marker(
          markerId: MarkerId("${el.latLng.latitude}-${el.latLng.longitude}"),
          icon: markerIcon,
          position: LatLng(el.latLng.latitude, el.latLng.longitude),
          onTap: () {
            Get.bottomSheet(
              BottomSheetWidget(
                data: el,
              ),
              elevation: 0,
            );
          }));
    });

    update();
  }

  getPosition() async {
    position.value = await geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
  }

  void clearSearchQuery() {
    suggestions.value = StaticListData.history;
  }

  listenPosition() {
    geolocator.getPositionStream(locationOptions).listen((Position ps) async {
      if (ps != null) {
//        LatLng latLng = LatLng(ps.latitude, ps.longitude);
        if (position.value == null ||
            (position.value.latitude != ps.latitude &&
                position.value.longitude != ps.longitude)) {
          print("position changed");
//          position.value = ps;

          setMapCameraPosition(defaultLatLng);

          isLocationChanged.value = true;
        }
      }
    });
  }
}
