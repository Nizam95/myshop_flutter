import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:get/get.dart';
import 'package:myshop_flutter/src/models/user_model.dart';
import 'package:myshop_flutter/locator.dart';
import 'package:myshop_flutter/src/constants/app_constant.dart';
import 'package:myshop_flutter/src/constants/route_path.dart';
import 'package:myshop_flutter/src/services/api_service.dart';
import 'package:myshop_flutter/src/services/shared_preferences_service.dart';
import 'package:myshop_flutter/src/utils/validator.dart';
import 'package:myshop_flutter/src/widgets/dialog_widget.dart';
import 'package:myshop_flutter/src/widgets/snackbar_widget.dart';

class AuthController extends GetxController {
  static AuthController getInstance = Get.find();

  AuthController() {
    localStorageService = locator<SharedPreferencesService>();
  }

  ///VARIABLE DECLARATION & INITIALIZATION
  SharedPreferencesService localStorageService;

  RxBool isLogged = false.obs;
  RxBool isLoading = false.obs;
  RxBool obscurePasswordText = true.obs;
  RxBool obscureNewPasswordText = true.obs;
  RxBool obscureConfirmPasswordText = true.obs;
  RxString token = ''.obs;
  Rx<UserModel> user = UserModel(
          fullName: '',
          idNumber: '',
          phoneNumber: '',
          idType: '',
          wallet: Wallet(currency: 'MYR', balance: '0.00'))
      .obs;
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  TextEditingController dobController = TextEditingController();
  TextEditingController phoneNumberController = TextEditingController();
  TextEditingController fullNameController = TextEditingController();

  TextEditingController newPasswordController;

  ///METHOD OVERRIDING
  @override
  void onReady() {
    ever(isLogged, handleAuthChanged);

    //to ensure context properly initialize and avoid call named route null
    //not sure why onReady not initializing instance correctly using schedulerBinding
    //need to explicitly call schedulerBinding
    SchedulerBinding.instance?.addPostFrameCallback(
        (_) => autoLogin().then((value) => isLogged.value = value));
    super.onInit();
  }

  @override
  void onClose() {
    usernameController?.dispose();
    passwordController?.dispose();
    confirmPasswordController?.dispose();
    dobController?.dispose();
    phoneNumberController?.dispose();
    fullNameController?.dispose();
    newPasswordController?.dispose();
    confirmPasswordController?.dispose();
    super.onClose();
  }

  ///PERFORM OTHER FUNCTION
  handleAuthChanged(token) {
    if (token == null || !token) {
      Get.offAllNamed(RoutePaths.LoginPage);
    } else {
      Get.offAllNamed(RoutePaths.MainPage);
    }
  }

  Future<bool> login() async {
    try {
      isLoading.value = true;
//      Map<String, dynamic> body = {
//        'username': usernameController.text,
//        'password': passwordController.text
//      };
//      final response =
//      await ApiService(path: 'authentication', body: body).postPublic();
//      final responseData = json.decode(response.body);

      isLoading.value = false;
//      if (response.statusCode != 201) {
//        await DialogWidget.errorDialog(body: responseData['message']);
//        return false;
//      }

//      user.value = UserModel.fromJson(responseData['user']);

//      token.value = responseData['accessToken'];

      user.value = UserModel(
        phoneNumber: "601123456789",
        fullName: "TESTING",
        wallet: Wallet(currency: "MYR", balance: "0.00"),
        birthDate: "01-01-1991",
        idNumber: "-",
        idType: "-",
        refCode: "-",
        username: "tester",
      );
      token.value = "value";
      final userData = json.encode(
        {
          'token': token.value,
        },
      );
      localStorageService.saveToLocalStorage(StorageKey.UserData, userData);
      isLogged.value = true;
      return true;
    } catch (error) {
      isLoading.value = false;
      isLogged.value = false;

      print(error.toString());
      throw error;
    }
  }

  Future<dynamic> requestOtp() async {
    try {
      isLoading.value = true;
      phoneNumberController.text =
          Validator.checkPhoneNumber(phoneNumberController.text);
      Map<String, dynamic> data = {
        "action": "verifyPhoneRequestOTP",
        "phoneNumber": '+60${phoneNumberController.text}',
      };
      final response =
          await ApiService(path: 'actions', body: data).patchPublic();
      final responseData = json.decode(response.body);

      isLoading.value = false;
      if (response.statusCode != 200) {
        SnackbarWidget.showError(responseData['message']);
      }

      return responseData['data']['OTP'];
    } catch (error) {
      isLoading.value = false;
      SnackbarWidget.showError(error.toString());
    }
  }

  Future<bool> submitOtp(String otp) async {
    try {
      isLoading.value = true;
      Map<String, dynamic> data = {
        "action": "verifyPhoneSubmitOTP",
        "phoneNumber": '+60${phoneNumberController.text}',
        "otp": otp
      };
      final response =
          await ApiService(path: 'actions', body: data).patchPublic();
      final responseData = json.decode(response.body);
      isLoading.value = false;
      if (response.statusCode != 200) {
        await DialogWidget.errorDialog(body: responseData['message']);
        return false;
      }

      return true;
    } catch (error) {
      isLoading.value = false;
      throw error;
    }
  }

  Future<bool> register() async {
    try {
      isLoading.value = true;
      Map<String, dynamic> data = {
        "action": "registerCustomer",
        "username": usernameController.text,
        "password": passwordController.text,
        "fullName": fullNameController.text,
        "birthDate": dobController.text,
        "phoneNumber": phoneNumberController.text,
      };

      final response =
          await ApiService(path: 'actions', body: data).patchPublic();
      final responseData = json.decode(response.body);
      isLoading.value = false;
      if (response.statusCode != 200) {
        await DialogWidget.errorDialog(body: responseData['message']);
        return false;
      }
      return true;
    } catch (error) {
      isLoading.value = false;
      throw error;
    }
  }

  Future<dynamic> forgotPassword(Map<String, dynamic> obj) async {
    try {
      final response =
          await ApiService(path: "actions", body: obj).patchPublic();
      final responseData = json.decode(response.body);

      if (response.statusCode != 200) {
        await DialogWidget.errorDialog(body: responseData['message']);
        return false;
      }

      return responseData;
    } catch (error) {
      print(error.toString());
      throw error;
    }
  }

  Future<bool> checkAuth() async {
    try {
//      var queryParameters = {
//        'action': 'token',
//      };
//      final response = await ApiService(path: 'customers', token: token.value)
//          .getPublic(parameters: queryParameters);
//      final responseData = json.decode(response.body);
//      if (response.statusCode != 200) {
//        return false;
//      } else {
//        user.value = UserModel.fromJson(responseData);
      user.value = UserModel(
        phoneNumber: "601123456789",
        fullName: "TESTING",
        wallet: Wallet(currency: "MYR", balance: "0.00"),
        birthDate: "01-01-1991",
        idNumber: "-",
        idType: "-",
        refCode: "-",
        username: "tester",
      );
      return true;
//      }
    } catch (error) {
      print(error.toString());
      throw error;
    }
  }

  Future<bool> autoLogin() async {
    try {
      //check first time user
      bool isFirstTime =
          localStorageService.getFromLocalStorage(StorageKey.IsFirstTime);
      if (isFirstTime == null || isFirstTime == true) {
        localStorageService.saveToLocalStorage(StorageKey.IsFirstTime, false);
        await logout();
        return false;
      }

      //check token from local storage if exist
      if (!localStorageService.checkKey(StorageKey.UserData)) {
        return false;
      } else {
        var userData =
            localStorageService.getFromLocalStorage(StorageKey.UserData);
        final extractedUserData = json.decode(userData) as Map<String, Object>;
        token.value = extractedUserData['token'];
        var isValidAuth = await checkAuth();

        if (!isValidAuth) {
          return false;
        } else
          return true;
      }
    } catch (error) {
      return false;
    }
  }

  void clearTextField() {
    usernameController.text = '';
    passwordController.text = '';
    confirmPasswordController.text = '';
    dobController.text = '';
    phoneNumberController.text = '';
    fullNameController.text = '';
  }

  Future<void> logout() async {
    token.value = null;
    isLogged.value = false;
    localStorageService.removeFromLocalStorage(StorageKey.UserData);
  }
}
