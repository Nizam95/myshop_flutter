import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myshop_flutter/src/models/push_notification_model.dart';
import 'package:myshop_flutter/src/widgets/snackbar_widget.dart';

class PushNotificationService {
  final FirebaseMessaging _fcm = FirebaseMessaging();
  PushNotificationModel notificationMessage;
  bool init = false;
  bool snackbarOpen = false;
  bool showMessage = false;

  bool get isShowMessage => showMessage;
  bool get isInit => init;
  bool get isSnackbarOpen => snackbarOpen;
  PushNotificationModel get getNotificationMessage => notificationMessage;

  Future<String> getToken() async {
    return await _fcm.getToken();
  }

  Future unsubscribeTopic(String topic) async {
    await _fcm.unsubscribeFromTopic(topic);
  }

  Future subscribeTopic(String topic) async {
    await _fcm.subscribeToTopic(topic);
  }

  Future deleteToken() async {
    await _fcm.deleteInstanceID();
  }

  void setNotificationMessage(Map<String, dynamic> message) {
    notificationMessage = PushNotificationModel.fromJson(message);
  }

  void setShowMessage(bool isShow) {
    showMessage = isShow;
  }

  Future<void> initialise() async {
    _fcm.configure(
      onMessage: (Map<String, dynamic> message) async {
        SnackbarWidget.showCustom(
          title: message['notification']['title'],
          message: message['notification']['body'],
          duration: message['data']['duration']
              ? Duration(seconds: int.parse(message['data']['duration']))
              : Duration(seconds: 5),
          backgroundColor: message['data']['color'] != null
              ? Color(int.parse(message['data']['color']))
              : Colors.green[600],
        );
      },
      // Called when the app has been closed comlpetely and it's opened
      // from the push notification.
      onLaunch: (Map<String, dynamic> message) async {
        print('onLaunch: $message');
        _serialiseAndNavigate(message);
      },
      // Called when the app is in the background and it's opened
      // from the push notification.
      onResume: (Map<String, dynamic> message) async {
        print('onResume: $message');
        _serialiseAndNavigate(message);
      },
    );
  }

  void onSnackbarChange(SnackStatus status) {
    switch (status) {
      case SnackStatus.SHOWING:
        {
          snackbarOpen = true;
          break;
        }
      case SnackStatus.IS_APPEARING:
        {
          snackbarOpen = true;
          break;
        }
      case SnackStatus.IS_HIDING:
        {
          break;
        }
      case SnackStatus.DISMISSED:
        {
          snackbarOpen = false;
          break;
        }
    }
  }

  void _serialiseAndNavigate(Map<String, dynamic> message) {
    var notificationData = message['data'];
    var view = notificationData['view'];

    if (view != null) {
      // Navigate to the create post view
    }
  }
}
