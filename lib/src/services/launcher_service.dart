import 'package:myshop_flutter/src/widgets/snackbar_widget.dart';
import 'package:url_launcher/url_launcher.dart';

class LauncherService {
  static Future<void> launchURL(String url) async {
    try {
      await launch(url);
    } catch (e) {
      SnackbarWidget.showError(e.toString());
    }
  }

  static Future<void> launchSocialMedia(String url) async {
    try {
      await launch(
        url,
        universalLinksOnly: true,
      );
    } catch (e) {
      SnackbarWidget.showError(e.toString());
    }
  }
}
