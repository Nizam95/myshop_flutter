import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:myshop_flutter/src/constants/app_data.dart';
import 'package:http/http.dart' as http;

import 'flavor_service.dart';

class ApiService {
  final String path;
  final token;
  var body;

  final String _baseUrl = FlavorService.instance.values.baseUrl;
  final String _imageUrl = FlavorService.instance.values.imageUrl;
  final String _url = FlavorService.instance.values.url;

  String get baseUrl => _baseUrl;
  String get imageUrl => _imageUrl;
  String get postUrl => "$_baseUrl/$path";

  ApiService({
    @required this.path,
    this.token,
    this.body,
  });

  Future<dynamic> getPlaces({String parameters}) async {
    try {
      final response =
          await http.get('http://photon.komoot.de/api/?q=$parameters');
      final body = json.decode(utf8.decode(response.bodyBytes));

      return body['features'];
    } on TimeoutException catch (_) {
      throw TimeoutException;
    } catch (error) {
      throw error;
    }
  }

  Future<dynamic> getPublic({Map<String, dynamic> parameters}) async {
    try {
      var queryParam = {};
      if (parameters != null) queryParam = parameters;

      var uri = Uri.https(_url, "/api/$path", queryParam);

      final response = await http.get(
        uri,
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
        },
      ).timeout(DefaultData.defaultFetchDuration);

      return response;
    } on TimeoutException catch (_) {
      throw TimeoutException;
    } catch (error) {
      throw error;
    }
  }

  Future<dynamic> getAuth(Map<String, dynamic> parameters) async {
    try {
      var queryParam = {};
      if (parameters != null) queryParam = parameters;

      var uri = Uri.https(_url, "/api/$path", queryParam);

      final response = await http.get(
        uri,
        headers: {
          HttpHeaders.authorizationHeader: token,
          HttpHeaders.contentTypeHeader: 'application/json',
        },
      ).timeout(DefaultData.defaultFetchDuration);

      return response;
    } on TimeoutException catch (_) {
      throw TimeoutException;
    } catch (error) {
      throw error;
    }
  }

  Future<dynamic> postPublic() async {
    try {
      final response = await http.post(
        postUrl,
        body: body,
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
        },
      ).timeout(DefaultData.defaultFetchDuration);

      return response;
    } on TimeoutException catch (_) {
      throw TimeoutException;
    } catch (error) {
      throw error;
    }
  }

  Future<dynamic> postAuth() async {
    try {
      final response = await http.post(
        postUrl,
        body: body,
        headers: {
          HttpHeaders.authorizationHeader: token,
          HttpHeaders.contentTypeHeader: 'application/json',
        },
      ).timeout(DefaultData.defaultFetchDuration);

      return response;
    } on TimeoutException catch (_) {
      throw TimeoutException;
    } catch (error) {
      throw error;
    }
  }

  Future<dynamic> patchPublic() async {
    try {
      final response = await http.patch(
        "$_baseUrl/$path",
        body: json.encode(body),
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
        },
      );
      return response;
    } on TimeoutException catch (_) {
      throw TimeoutException;
    } catch (error) {
      print(error.toString());
      throw error;
    }
  }

  Future<dynamic> patchAuth() async {
    try {
      final response = await http.patch(
        "$_baseUrl/$path",
        body: json.encode(body),
        headers: {
          HttpHeaders.authorizationHeader: token,
          HttpHeaders.contentTypeHeader: 'application/json',
        },
      );
      return response;
    } on TimeoutException catch (_) {
      throw TimeoutException;
    } catch (error) {
      throw error;
    }
  }

  Future<dynamic> postFormData(
      {File file, String name, Map<String, dynamic> data}) async {
    try {
      List<int> imageBytes = file.readAsBytesSync();
      String base64Image = base64Encode(imageBytes);

      Map<String, dynamic> body = {
        "name": name,
        "file": "data:image/png;base64,${base64Image.toString()}",
        ...data
      };

      final response = await http.post("$_baseUrl/$path",
          headers: {HttpHeaders.authorizationHeader: token}, body: body);

      return response;
    } on TimeoutException catch (_) {
      throw TimeoutException;
    } catch (error) {
      throw error;
    }
  }
}
