import 'package:flutter/material.dart';
import 'package:myshop_flutter/src/constants/enum_constant.dart';
import 'package:myshop_flutter/src/utils/utils.dart';

class FlavorService {
  final Flavor flavor;
  final String name;
  final FlavorValues values;
  static FlavorService _instance;

  factory FlavorService(
      {@required Flavor flavor, @required FlavorValues values}) {
    _instance ??= FlavorService._internal(
        flavor, Util.enumName(flavor.toString()), values);
    return _instance;
  }

  FlavorService._internal(this.flavor, this.name, this.values);
  static FlavorService get instance {
    return _instance;
  }

  /// Add your new flavor here
  static bool isProduction() => _instance.flavor == Flavor.production;
  static bool isDevelopment() => _instance.flavor == Flavor.dev;
  static bool isStaging() => _instance.flavor == Flavor.staging;
}

///setup your specific environment value here
class FlavorValues {
  FlavorValues(
      {@required this.baseUrl,
      @required this.url,
      this.addressUrl,
      this.imageUrl,
      this.reCaptchaKey});
  final String url;
  final String baseUrl;
  final String addressUrl;
  final String imageUrl;
  final String reCaptchaKey;
//Add other flavor specific values, e.g database name
}
