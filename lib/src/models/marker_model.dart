import 'package:google_maps_flutter/google_maps_flutter.dart';

class MarkerModel {
  LatLng latLng;
  String name;
  String description;
  String websiteUrl;
  String phoneNumber;
  String address;

  MarkerModel(
      {this.latLng,
      this.name,
      this.description,
      this.websiteUrl,
      this.phoneNumber,
      this.address});
}
