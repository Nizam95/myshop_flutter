class PushNotificationModel {
  NotificationData notification;

  PushNotificationModel({this.notification});

  PushNotificationModel.fromJson(Map<String, dynamic> json) {
    notification = json['notification'] != null
        ? new NotificationData.fromJson(json['notification'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.notification != null) {
      data['notification'] = this.notification.toJson();
    }

    return data;
  }
}

class NotificationData {
  String title;
  String body;

  NotificationData({this.title, this.body});

  NotificationData.fromJson(Map<dynamic, dynamic> json) {
    title = json['title'];
    body = json['body'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    data['body'] = this.body;
    return data;
  }
}
