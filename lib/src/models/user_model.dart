//user placeholder
class UserModel {
  String sId;
  String username;
  String phoneNumber;
  String fullName;
  String birthDate;
  String idNumber;
  String idType;
  Wallet wallet;
  String refCode;
  String createdAt;
  String updatedAt;

  UserModel(
      {this.sId,
      this.username,
      this.phoneNumber,
      this.fullName,
      this.birthDate,
      this.idNumber,
      this.idType,
      this.wallet,
      this.refCode,
      this.createdAt,
      this.updatedAt});

  UserModel.fromJson(Map<String, dynamic> json) {
    sId = json['_id'].toString();
    username = json['username'].toString();
    phoneNumber = json['phoneNumber'].toString();
    fullName = json['fullName'].toString();
    birthDate = json['birthDate'].toString();
    idNumber = json['idNumber'].toString();
    idType = json['idType'].toString();
    wallet =
        json['wallet'] != null ? new Wallet.fromJson(json['wallet']) : null;
    refCode = json['refCode'].toString();
    createdAt = json['createdAt'].toString();
    updatedAt = json['updatedAt'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['username'] = this.username;
    data['phoneNumber'] = this.phoneNumber;
    data['fullName'] = this.fullName;
    data['birthDate'] = this.birthDate;
    data['idNumber'] = this.idNumber;
    data['idType'] = this.idType;
    if (this.wallet != null) {
      data['wallet'] = this.wallet.toJson();
    }
    data['refCode'] = this.refCode;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    return data;
  }
}

class Wallet {
  String balance;
  String currency;

  Wallet({this.balance, this.currency});

  Wallet.fromJson(Map<String, dynamic> json) {
    balance = json['balance'].toString();
    currency = json['currency'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['balance'] = this.balance.toString();
    data['currency'] = this.currency.toString();
    return data;
  }
}
