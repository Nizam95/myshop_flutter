import 'package:flutter/material.dart';

class GeometryModel {
  String latitude;
  String longitude;
  String type;

  GeometryModel({this.latitude, this.longitude, this.type});

  GeometryModel.fromJson(Map<String, dynamic> json) {
    latitude = json['coordinates'][0].toString();
    longitude = json['coordinates'][1].toString();
    type = json['type'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['latitude'] = this.latitude.toString();
    data['longitude'] = this.longitude.toString();
    data['type'] = this.type.toString();
    return data;
  }
}

class PlaceModel {
  final String name;
  final String state;
  final String country;
  final GeometryModel geometry;

  const PlaceModel({
    @required this.name,
    this.state,
    this.geometry,
    @required this.country,
  })  : assert(name != null),
        assert(country != null);

  bool get hasState => state?.isNotEmpty == true;
  bool get hasCountry => country?.isNotEmpty == true;

  bool get isCountry => hasCountry && name == country;
  bool get isState => hasState && name == state;

  factory PlaceModel.fromJson(Map<String, dynamic> map) {
    final props = map['properties'];
    return PlaceModel(
        name: props['name'] ?? '',
        state: props['state'] ?? '',
        country: props['country'] ?? '',
        geometry: GeometryModel.fromJson(
          map['geometry'],
        ));
  }

  String get address {
    if (isCountry) return country;
    return '$name, $level2Address';
  }

  String get addressShort {
    if (isCountry) return country;
    return '$name, $country';
  }

  String get level2Address {
    if (isCountry || isState || !hasState) return country;
    if (!hasCountry) return state;
    return '$state, $country';
  }

  @override
  String toString() =>
      'PlaceModel(name: $name, state: $state, country: $country)';

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is PlaceModel &&
        o.name == name &&
        o.state == state &&
        o.country == country;
  }

  @override
  int get hashCode => name.hashCode ^ state.hashCode ^ country.hashCode;
}
