import 'package:flutter/cupertino.dart';

class WidgetListModel {
  Widget icon;
  String title;
  dynamic value;
  String route;
  Object arguments;

  WidgetListModel(
      {this.icon, this.title, this.value, this.route, this.arguments});
}
