import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_fullpdfview/flutter_fullpdfview.dart';
import 'package:myshop_flutter/src/constants/app_theme_constant.dart';
import 'package:myshop_flutter/src/widgets/snackbar_widget.dart';

class PDFViewerWidget extends StatefulWidget {
  final FileInfo fileInfo;
  PDFViewerWidget({Key key, this.fileInfo}) : super(key: key);

  _PdfViewerWidgetState createState() => _PdfViewerWidgetState();
}

class _PdfViewerWidgetState extends State<PDFViewerWidget> {
  int pages = 0;
  bool isReady = false;
  String errorMessage = '';
  GlobalKey pdfKey = GlobalKey();
  Completer<PDFViewController> _controller = Completer<PDFViewController>();

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey,
      child: Stack(
        children: <Widget>[
          PDFView(
            key: pdfKey,
            displayAsBook: true,
            filePath: widget.fileInfo.file.path,
            fitEachPage: true,
            fitPolicy: FitPolicy.BOTH,
            dualPageMode: false,
            enableSwipe: true,
            swipeHorizontal: true,
            autoSpacing: true,
            backgroundColor: bgcolors.WHITE,
            pageFling: true,
            pageSnap: true,
            onRender: (_pages) {
              setState(() {
                pages = _pages;
                isReady = true;
              });
            },
            onError: (error) {
              setState(() {
                errorMessage = error.toString();
              });
            },
            onPageError: (page, error) {
              setState(() {
                errorMessage = '$page: ${error.toString()}';
              });
            },
            onViewCreated: (PDFViewController pdfViewController) {
              _controller.complete(pdfViewController);
            },
            onPageChanged: (int page, int total) {},
          ),
          FutureBuilder<PDFViewController>(
            future: _controller.future,
            builder: (context, AsyncSnapshot<PDFViewController> snapshot) {
              if (snapshot.hasData) {
                return SafeArea(
                  child: Align(
                    alignment: Alignment.bottomRight,
                    child: Padding(
                      padding: AppTheme.paddingBig,
                      child: FloatingActionButton(
                        child: Icon(Icons.arrow_forward_ios),
                        onPressed: () async {
                          try {
                            await snapshot.data.setPage(2);
                          } catch (e) {
                            SnackbarWidget.showError(e.toString());
                          }
                        },
                      ),
                    ),
                  ),
                );
              }

              return Container();
            },
          ),
          errorMessage.isEmpty
              ? !isReady
                  ? Center(
                      child: CircularProgressIndicator(),
                    )
                  : Container()
              : Center(
                  child: Text(errorMessage),
                ),
        ],
      ),
    );
  }
}
