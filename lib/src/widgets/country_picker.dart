import 'package:country_pickers/country.dart';
import 'package:country_pickers/country_picker_dialog.dart';
import 'package:country_pickers/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:myshop_flutter/src/constants/app_theme_constant.dart';

class CountryPicker {
  static Future<void> openCountryDialog({
    BuildContext context,
    ValueChanged callback,
    List<String> itemFilter,
  }) =>
      showDialog(
        context: context,
        builder: (context) => Theme(
            data: Theme.of(context).copyWith(primaryColor: Colors.pink),
            child: CountryPickerDialog(
                titlePadding: AppTheme.paddingSmall,
                searchCursorColor: Colors.pinkAccent,
                searchInputDecoration: InputDecoration(
                    hintText:
                        '${FlutterI18n.translate(context, "text_label.search")}...'),
                isSearchable: true,
                itemFilter: (Country val) {
                  if (itemFilter != null && itemFilter.length > 0) {
                    return itemFilter.contains(val.isoCode);
                  } else
                    return true;
                },
                title: Text(
                    FlutterI18n.translate(context, "text_label.selectCountry")),
                onValuePicked: callback,
                itemBuilder: _countryDialogItem)),
      );

  static Widget _countryDialogItem(Country country) => Row(
        children: <Widget>[
          CountryPickerUtils.getDefaultFlagImage(country),
          AppTheme.dividerHorizontalSmall,
          Flexible(child: Text(country.name))
        ],
      );
}
