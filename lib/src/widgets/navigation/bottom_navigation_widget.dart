import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:myshop_flutter/src/constants/app_constant.dart';
import 'package:myshop_flutter/src/constants/app_theme_constant.dart';
import 'package:myshop_flutter/src/constants/assets_path.dart';
import 'package:myshop_flutter/src/views/main/activity/activity_view.dart';
import 'package:myshop_flutter/src/views/main/home/home_view.dart';
import 'package:myshop_flutter/src/views/main/more/more_view.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class BottomNavigationWidget {
  BottomNavigationBarItem buildBottomNavigationBarItem({
    String image,
    String title,
  }) {
    return BottomNavigationBarItem(
      backgroundColor: Colors.white,
      icon: Image.asset(
        image,
        height: 30,
      ),
      title: Padding(
        padding: const EdgeInsets.only(top: 8),
        child: Text(
          title,
        ),
      ),
    );
  }

  PersistentBottomNavBarItem buildPersistentBottomNavigationBarItem({
    String image,
    String title,
  }) {
    return PersistentBottomNavBarItem(
      icon: Image.asset(
        image,
      ),
      contentPadding: 0,
      activeContentColor: AppTheme.primaryAppColor,
      titleFontSize: AppTheme.captionFontSize,
      title: title,
      activeColor: AppTheme.textPrimaryColor,
      inactiveColor: CupertinoColors.systemGrey,
    );
  }

  List<BottomNavigationBarItem> getBottomNavigationBarItems() {
    List<BottomNavigationBarItem> bottomNavigationBarItems = [
      BottomNavigationWidget().buildBottomNavigationBarItem(
        title: AppConstant.Tab1,
        image: AssetsPath.AppLogo,
      ),
      BottomNavigationWidget().buildBottomNavigationBarItem(
        title: AppConstant.Tab2,
        image: AssetsPath.AppLogo,
      ),
    ];
    return bottomNavigationBarItems;
  }

  List<PersistentBottomNavBarItem> getPersistentBottomNavigationBarItems() {
    List<PersistentBottomNavBarItem> bottomNavigationBarItems = [
      BottomNavigationWidget().buildPersistentBottomNavigationBarItem(
        title: AppConstant.Tab1,
        image: AssetsPath.AppLogo,
      ),
      BottomNavigationWidget().buildPersistentBottomNavigationBarItem(
        title: AppConstant.Tab2,
        image: AssetsPath.AppLogo,
      ),
    ];
    return bottomNavigationBarItems;
  }

  Widget getCurrentBottomNavigationBarView({int index}) {
    switch (index) {
      case 0:
        return HomePage();
      case 1:
        return MorePage();

      default:
        return HomePage();
    }
  }
}
