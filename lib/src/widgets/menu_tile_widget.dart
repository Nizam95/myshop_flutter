import 'package:get/get.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'beautiful_popup_widget.dart';
import 'package:myshop_flutter/src/models/widget_list_model.dart';

class MenuTileWidget extends StatelessWidget {
  final WidgetListModel menu;
  final double width;
  final double height;
  final EdgeInsetsGeometry padding;
  final EdgeInsetsGeometry margin;
  final double fontSize;
  final Color textColor;
  final EdgeInsetsGeometry textPadding;
  final double dividerHeight;
  final bool useDivider;
  const MenuTileWidget(
      {Key key,
      this.menu,
      this.width,
      this.height,
      this.padding,
      this.margin,
      this.fontSize,
      this.textColor,
      this.textPadding,
      this.dividerHeight,
      this.useDivider = false})
      : super(key: key);

  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => menu.route != null
          ? handleRoute(menu.route)
          : BeautifulPopupWidget.openComingSoonDialog(context: context),
      child: Container(
        width: width != null ? width : Get.width / 3,
        height: height != null ? height : Get.width / 3,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(child: menu.icon),
            SizedBox(
              height: dividerHeight != null ? dividerHeight : 8,
            ),
            Flexible(
              child: Padding(
                padding: textPadding != null
                    ? textPadding
                    : EdgeInsets.symmetric(horizontal: 16),
                child: Text(
                  menu.title,
                  style: TextStyle(
                    color: textColor != null ? textColor : Colors.black,
                    fontSize: fontSize != null
                        ? fontSize
                        : Get.textTheme.subtitle1.fontSize,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            SizedBox(
              height: 12,
            ),
            useDivider
                ? Divider(
                    color: Get.theme.primaryColor,
                    thickness: 2,
                    indent: 40,
                    endIndent: 40,
                  )
                : Container(),
          ],
        ),
      ),
    );
  }

  void handleRoute(String route) {
    Get.toNamed(menu.route, arguments: menu.arguments);

    ///INTENT EXAMPLE --UNCOMMENT THIS TO OPEN OTHER APP
//      android_intent.Intent()
//        ..setAction(android_action.Action.ACTION_SEND)
//        ..setPackage(AppConstant.AppId)
//        ..setType('text/plain')
//        ..putExtra(
//            android_extra.Extra.EXTRA_TEXT,
//            json.encode({
//              'referralCode': 'myshop',
//              'referralType': 'business'
//            }).toString())
//        ..startActivity().catchError((e) {
//          if (e is PlatformException) {
//            LauncherService.launchURL(UrlConstant.PlayStoreURL);
//          } else {
//            SnackbarWidget.showError(e.toString());
//          }
//        });
  }
}
