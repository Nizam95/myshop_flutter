import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myshop_flutter/src/models/widget_list_model.dart';

import 'menu_tile_widget.dart';

class CustomAppBarWidget extends SliverPersistentHeaderDelegate {
  final double expandedHeight;
  final String appbarTitle;
  final Widget iconWidget;
  final String menuTitle;
  final bool useDivider;
  final double dividerHeight;

  CustomAppBarWidget(
      {@required this.expandedHeight,
      this.appbarTitle,
      this.iconWidget,
      this.menuTitle,
      this.useDivider = true,
      this.dividerHeight});

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    var inOpacity = shrinkOffset / expandedHeight - 0.3;
    var outOpacity = shrinkOffset / expandedHeight - 0.6;
    return Stack(
      fit: StackFit.expand,
      overflow: Overflow.visible,
      children: [
        Center(
          child: Opacity(
              opacity:
                  inOpacity < 0.0 ? 0.0 : inOpacity == 0.7 ? 1.0 : inOpacity,
              child: AppBar(
                backgroundColor: Colors.white,
                title: Text(
                  appbarTitle,
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
              )),
        ),
        Opacity(
            opacity: (1 - outOpacity) > 1.0 && (1 - outOpacity) < 1.4
                ? (1 - shrinkOffset / expandedHeight)
                : outOpacity < 0.0 ? 1.0 : 0.0,
            child: MenuTileWidget(
              menu: WidgetListModel(
                icon: iconWidget,
                title: menuTitle,
              ),
              fontSize: Get.textTheme.headline5.fontSize,
              height: 200,
              width: Get.width,
              useDivider: useDivider ? useDivider : true,
              dividerHeight: dividerHeight != null ? dividerHeight : 16,
            )),
      ],
    );
  }

  @override
  double get maxExtent => expandedHeight;

  @override
  double get minExtent => kToolbarHeight;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) => true;
}
