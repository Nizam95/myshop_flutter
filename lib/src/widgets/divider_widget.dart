import 'package:flutter/material.dart';
import 'package:myshop_flutter/src/constants/app_theme_constant.dart';

class DividerWidget extends StatelessWidget {
  final EdgeInsetsGeometry padding;

  const DividerWidget({Key key, this.padding}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding != null ? padding : AppTheme.paddingBig,
      child: Divider(
        color: AppTheme.grey,
      ),
    );
  }
}
