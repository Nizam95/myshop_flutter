import 'package:flutter/cupertino.dart';
import 'package:flutter_beautiful_popup/main.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:get/get.dart';
import 'package:myshop_flutter/src/widgets/template/comping_soon_template.dart';

class BeautifulPopupWidget {
  static void openComingSoonDialog({BuildContext context}) async {
    final popup = BeautifulPopup.customize(
      context: context,
      build: (options) => TemplateComingSoon(options),
    );
    popup.show(
      title: FlutterI18n.translate(context, "title.comingSoon"),
      content: FlutterI18n.translate(context, "info_msg.contentComingSoon"),
      barrierDismissible: true,
      actions: [
        popup.button(
            label: FlutterI18n.translate(context, "btn_label.close")
                ?.toUpperCase(),
            onPressed: () => Get.back()),
      ],
    );
  }
}
