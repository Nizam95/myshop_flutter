import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myshop_flutter/src/constants/app_theme_constant.dart';
import 'package:myshop_flutter/src/widgets/button/fab_button_widget.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class PageContainerWidget extends StatelessWidget {
  final RxBool inAsyncCall;
  final bool showBackButton;
  final Function onPressedBackButton;
  final GlobalKey<FormState> formKey;
  final List<Widget> widgetList;
  final Widget buttonWidget;

  const PageContainerWidget(
      {Key key,
      this.inAsyncCall,
      this.showBackButton = false,
      this.onPressedBackButton,
      this.formKey,
      this.widgetList,
      this.buttonWidget})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () => Get.focusScope.unfocus(),
        child: Obx(
          () => ModalProgressHUD(
            inAsyncCall: inAsyncCall.value,
            child: Stack(
              children: <Widget>[
                Container(
                  height: Get.height,
                  width: Get.width,
                  decoration: AppTheme.gradientBackground,
                ),
                Container(
                  height: Get.height,
                  width: Get.width,
                  child: SingleChildScrollView(
                    physics: AlwaysScrollableScrollPhysics(),
                    padding: AppTheme.containerPadding,
                    child: Form(
                      key: formKey,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: widgetList),
                    ),
                  ),
                ),
                showBackButton
                    ? FabButtonWidget(
                        onPressed: onPressedBackButton,
                      )
                    : Container(),
                MediaQuery.of(context).viewInsets.bottom != 0.0
                    ? Container()
                    : buttonWidget
              ],
            ),
          ),
        ),
      ),
    );
  }
}
