import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:get/get.dart';
import 'package:myshop_flutter/src/constants/enum_constant.dart';
import 'package:myshop_flutter/src/constants/app_theme_constant.dart';

class DialogWidget {
  static Future<DialogAction> yesAbortDialog(
      {String title, String body, String btnYes, String btnNo}) async {
    Widget content = AlertDialog(
      title: Text(title),
      content: Text(
        body,
      ),
      actions: <Widget>[
        FlatButton(
          onPressed: () => Get.back(result: DialogAction.abort),
          child: Text(
            btnNo.toUpperCase(),
          ),
        ),
        RaisedButton(
          color: Get.theme.primaryColor,
          onPressed: () => Get.back(result: DialogAction.yes),
          child: Text(
            btnYes.toUpperCase(),
            style: TextStyle(
              color: Colors.white,
            ),
          ),
        ),
      ],
    );
    final action = await Get.dialog(content, barrierDismissible: false);

    return (action != null) ? action : DialogAction.abort;
  }

  static Future<DialogAction> errorDialog({String title, String body}) async {
    Widget content = AlertDialog(
      title: Text(title != null ? title : 'Error'),
      content: Text(body),
      actions: <Widget>[
        FlatButton(
          onPressed: () => Get.back(result: DialogAction.abort),
          child: const Text('OK'),
        ),
      ],
    );
    final action = await Get.dialog(content, barrierDismissible: false);

    return (action != null) ? action : DialogAction.abort;
  }

  static Future<DialogAction> successDialog({String title, String body}) async {
    Widget content = AlertDialog(
      title: Text(title != null ? title : 'Success'),
      content: Text(body),
      actions: <Widget>[
        FlatButton(
          onPressed: () => Get.back(result: DialogAction.success),
          child: const Text('OK'),
        ),
      ],
    );
    final action = await Get.dialog(content, barrierDismissible: false);

    return (action != null) ? action : DialogAction.success;
  }

  static Future<ImageStatus> openImagePickerDialog() async {
    Widget content = SimpleDialog(
      children: <Widget>[
        Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            RaisedButton(
              onPressed: () {
                Get.back(result: ImageStatus.Camera);
              },
              child: Text('Take a picture from camera'),
            ),
            RaisedButton(
              onPressed: () {
                Get.back(result: ImageStatus.Camera);
              },
              child: Text('Select image from gallery'),
            )
          ],
        ),
      ],
    );
    final action = await Get.dialog(content, barrierDismissible: false);

    return (action != null) ? action : null;
  }

  static Widget onLoading(String loadingMessage) {
    return Container(
      alignment: AlignmentDirectional.center,
      decoration: BoxDecoration(
        color: Colors.white70,
      ),
      child: Container(
        decoration: BoxDecoration(
            color: AppTheme.primaryDarkColor,
            borderRadius: BorderRadius.circular(10.0)),
        width: 150.0,
        height: 150.0,
        alignment: AlignmentDirectional.center,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Center(
              child: SizedBox(
                height: 50.0,
                width: 50.0,
                child: CircularProgressIndicator(
                  value: null,
                  strokeWidth: 7.0,
                  backgroundColor: Colors.white,
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 25.0),
              child: Center(
                child: Text(
                  loadingMessage,
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  static Future<DialogAction> confirmationBoxErrorWidget(
      {BuildContext context}) async {
    return await errorDialog(
        body: FlutterI18n.translate(
            context, "error_msg.pleaseCheckConfirmationBox"));
  }

  static Future<DialogAction> generalErrorWidget({BuildContext context}) async {
    return await errorDialog(
        body: FlutterI18n.translate(context, "error_msg.generalError"));
  }

  static Future<DialogAction> fieldErrorWidget({BuildContext context}) async {
    return await errorDialog(
        body: FlutterI18n.translate(context, "error_msg.generalFieldError"));
  }

  static Future<DialogAction> leavePageDialogWidget(
      {BuildContext context}) async {
    return await yesAbortDialog(
        title: FlutterI18n.translate(context, "title.confirmation"),
        btnNo:
            FlutterI18n.translate(context, "btn_label.cancel")?.toUpperCase(),
        btnYes: FlutterI18n.translate(context, "btn_label.yes")?.toUpperCase(),
        body: FlutterI18n.translate(context, "info_msg.leavePageConfirmation"));
  }

  static Future<DialogAction> discardDialogWidget(
      {BuildContext context}) async {
    return await yesAbortDialog(
        title: FlutterI18n.translate(context, "title.confirmation"),
        btnNo: FlutterI18n.translate(context, "btn_label.no")?.toUpperCase(),
        btnYes: FlutterI18n.translate(context, "btn_label.yes")?.toUpperCase(),
        body: FlutterI18n.translate(context, "info_msg.leavePageConfirmation"));
  }

  static Future<DialogAction> logoutDialogWidget({BuildContext context}) async {
    return await DialogWidget.yesAbortDialog(
        title: FlutterI18n.translate(context, "title.confirmation"),
        btnNo:
            FlutterI18n.translate(context, "btn_label.cancel")?.toUpperCase(),
        btnYes: FlutterI18n.translate(context, "btn_label.yes")?.toUpperCase(),
        body: FlutterI18n.translate(context, "info_msg.logoutConfirmation"));
  }
}
