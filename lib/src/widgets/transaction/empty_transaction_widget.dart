import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:myshop_flutter/src/constants/app_theme_constant.dart';

class EmptyTransactionWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Flexible(
            child: Padding(
              padding: EdgeInsets.symmetric(
                  vertical: AppTheme.paddingHorizontalBig,
                  horizontal: AppTheme.paddingHorizontalBig),
              child: FittedBox(
                fit: BoxFit.fill,
                child: Icon(
                  Icons.remove_circle_outline,
                  color: AppTheme.grey,
                  size: 72,
                ),
              ),
            ),
          ),
          Flexible(
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: AppTheme.paddingHorizontalBig),
              child: FittedBox(
                fit: BoxFit.fitWidth,
                child: Text(
                  FlutterI18n.translate(context, "info_msg.youHaveNoTxn"),
                  style: TextStyle(
                    color: AppTheme.grey,
                    fontSize: AppTheme.bigTitleFontSize,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
