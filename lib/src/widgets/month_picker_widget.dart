import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:myshop_flutter/src/constants/app_theme_constant.dart';
import 'package:month_picker_dialog/month_picker_dialog.dart';

class MonthPickerWidget extends StatelessWidget {
  final Function callback;
  final String month;
  final String year;

  const MonthPickerWidget({Key key, this.callback, this.month, this.year})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: AppTheme.paddingHorizontalBig),
      child: Card(
        elevation: AppTheme.elevationBig,
        shape: AppTheme.roundBorderSmall,
        child: Padding(
          padding:
              EdgeInsets.symmetric(vertical: AppTheme.paddingVerticalSmall),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: AppTheme.paddingHorizontalBig),
                  child: InkWell(
                    onTap: () async {
                      DateTime date = await showMonthPicker(
                          context: context, initialDate: DateTime.now());

                      callback(date);
                    },
                    child: Column(
                      children: <Widget>[
                        Text(
                          FlutterI18n.translate(context, "text_label.month"),
                          style: TextStyle(fontSize: AppTheme.titleFontSize),
                        ),
                        AppTheme.dividerVerticalSmall,
                        Text(
                          month,
                          style: TextStyle(fontSize: AppTheme.titleFontSize),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(
                    vertical: AppTheme.paddingHorizontalSmall),
                height: 60,
                child: VerticalDivider(color: Colors.black),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: AppTheme.paddingHorizontalBig),
                  child: InkWell(
                    onTap: () async {
                      DateTime date = await showMonthPicker(
                          context: context, initialDate: DateTime.now());

                      callback(date);
                    },
                    child: Column(
                      children: <Widget>[
                        Text(
                          FlutterI18n.translate(context, "text_label.year"),
                          style: TextStyle(fontSize: AppTheme.titleFontSize),
                        ),
                        AppTheme.dividerVerticalSmall,
                        Text(
                          year,
                          style: TextStyle(fontSize: AppTheme.titleFontSize),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
