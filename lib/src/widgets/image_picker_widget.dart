import 'dart:io';

import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart' as syspaths;
import 'package:myshop_flutter/src/constants/enum_constant.dart';

class ImagePickerWidget {
  final ImagePicker _picker = ImagePicker();

  Future<File> takePicture(ImageStatus image) async {
    try {
      PickedFile imageFile;

      if (image == ImageStatus.Camera) {
        imageFile = await _picker.getImage(
          source: ImageSource.camera,
          maxWidth: 800,
          maxHeight: 800,
          imageQuality: 85,
        );
      } else if (image == ImageStatus.Gallery) {
        imageFile = await _picker.getImage(
          source: ImageSource.gallery,
          maxWidth: 800,
          maxHeight: 800,
          imageQuality: 85,
        );
      }

      final appDir = await syspaths.getApplicationDocumentsDirectory();
      final fileName = path.basename(imageFile.path);
      final savedImage =
          await File(imageFile.path).copy('${appDir.path}/$fileName');

      return savedImage;
    } catch (e) {
      return null;
    }
  }
}
