import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:get/get.dart';
import 'package:myshop_flutter/src/constants/app_theme_constant.dart';
import 'package:myshop_flutter/src/constants/assets_path.dart';
import 'package:myshop_flutter/src/constants/enum_constant.dart';
import 'package:myshop_flutter/src/constants/route_path.dart';
import 'package:myshop_flutter/src/controllers/auth_controller.dart';

import 'beautiful_popup_widget.dart';
import 'dialog_widget.dart';

class DrawerWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        color: Colors.white,
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: 16,
              ),
              Container(
                color: Colors.white,
                width: Get.width,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: AppTheme.paddingBig,
                      child: CircleAvatar(
                        backgroundImage: AssetImage(AssetsPath.AppLogo),
                        radius: 30,
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          AuthController.getInstance.user.value.username != null
                              ? AuthController.getInstance.user.value.username
                                  .toUpperCase()
                              : '',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: Get.textTheme.headline6.fontSize,
                              color: Colors.black),
                        ),
                        AppTheme.dividerVerticalExtraSmall,
                        Text(
                          AuthController.getInstance.user.value.phoneNumber !=
                                  null
                              ? AuthController
                                  .getInstance.user.value.phoneNumber
                              : '',
                          style: TextStyle(
                              fontSize: Get.textTheme.bodyText1.fontSize,
                              color: Colors.black),
                        ),
                        AppTheme.dividerVerticalExtraSmall,
                      ],
                    ),
                  ],
                ),
              ),
              Expanded(
                child: ListView(
                  children: <Widget>[
                    AppTheme.dividerVerticalSmall,
                    AppTheme.sidebarDivider,
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 16, left: 20, bottom: 8, right: 20),
                      child: Text(
                        FlutterI18n.translate(context, "title.quickLink")
                            ?.toUpperCase(),
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: Get.textTheme.subtitle1.fontSize,
                            color: Get.theme.primaryColor),
                      ),
                    ),
                    buildListTile(
                        title: FlutterI18n.translate(
                            context, "title.notification"),
                        route: RoutePaths.NotificationPage,
                        context: context),
//                    buildListTile(
//                        title: "Reports", route: null, context: context),
//                    buildListTile(
//                        title: "News & Events", route: null, context: context),
//                    buildListTile(
//                        title: "Training", route: null, context: context),
                    buildListTile(
                        title: FlutterI18n.translate(context, "title.faq"),
                        route: null,
                        context: context),
                    AppTheme.dividerVerticalSmall,
                    AppTheme.sidebarDivider,
//                    Padding(
//                      padding: const EdgeInsets.only(
//                          top: 16, left: 20, bottom: 8, right: 20),
//                      child: Text(
//                        'CUSTOMER SUPPORT',
//                        style: TextStyle(
//                            fontWeight: FontWeight.bold,
//                            fontSize: Get.textTheme.subtitle1.fontSize,
//                            color: Get.theme.primaryColor),
//                      ),
//                    ),
//                    buildListTile(
//                        title: "Live Chat", route: null, context: context),
//                    AppTheme.dividerVerticalSmall,
//                    Divider(
//                      color: Colors.grey,
//                      thickness: 1,
//                      indent: 20,
//                      endIndent: 20,
//                    ),
//                    Padding(
//                      padding: const EdgeInsets.only(
//                          top: 16, left: 20, bottom: 8, right: 20),
//                      child: Text(
//                        'QUICK LINKS',
//                        style: TextStyle(
//                            fontWeight: FontWeight.bold,
//                            fontSize: Get.textTheme.subtitle1.fontSize,
//                            color: Get.theme.primaryColor),
//                      ),
//                    ),
                    buildListTile(
                        title: FlutterI18n.translate(context, "title.profile"),
                        route: RoutePaths.ProfilePage,
                        context: context),
                    buildListTile(
                        title: FlutterI18n.translate(
                            context, "title.changePassword"),
                        route: RoutePaths.ChangePasswordPage,
                        context: context),
                    buildListTile(
                        title: FlutterI18n.translate(context, "title.logout"),
                        route: RoutePaths.LogoutPage,
                        context: context),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget buildListTile({String title, String route, BuildContext context}) {
    return GestureDetector(
      onTap: () async {
        if (route == null) {
          BeautifulPopupWidget.openComingSoonDialog(context: context);
        } else if (route == RoutePaths.LogoutPage) {
          DialogAction result =
              await DialogWidget.logoutDialogWidget(context: context);
          if (result == DialogAction.yes) {
            await AuthController.getInstance.logout();
          }
        } else {
          Get.toNamed(route);
        }
      },
      child: Padding(
        padding: EdgeInsets.only(top: 4, right: 8, bottom: 8, left: 48),
        child: Text(
          title,
          style: TextStyle(
              fontWeight: FontWeight.w400,
              color: Colors.black,
              fontSize: Get.textTheme.subtitle1.fontSize),
        ),
      ),
    );
  }
}
