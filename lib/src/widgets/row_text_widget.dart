import 'package:flutter/material.dart';

class RowTextWidget extends StatelessWidget {
  final String title;
  final String value;

  RowTextWidget({
    this.title,
    this.value,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 4.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Expanded(
            flex: 5, // takes 50% of available width
            child: Text(
              title,
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
          ),
          Expanded(
            flex: 5, // takes 50% of available width
            child: Text(
              value != null ? value : '',
              textAlign: TextAlign.end,
            ),
          ),
        ],
      ),
    );
  }
}
