import 'package:country_pickers/country.dart';
import 'package:myshop_flutter/src/constants/app_constant.dart';
import 'package:myshop_flutter/src/constants/app_theme_constant.dart';
import 'package:myshop_flutter/src/constants/messages_constant.dart';
import 'package:myshop_flutter/src/widgets/country_picker.dart';
import 'package:phone_number/phone_number.dart';
import 'package:country_pickers/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:myshop_flutter/src/utils/validator.dart';

class PhoneFieldIntlWidget extends StatefulWidget {
  final bool autoValidate;
  final ValueChanged<String> onSaved;
  final Country selectedCountry;
  final ValueChanged<String> onFieldSubmitted;
  final TextEditingController controller;
  final Function callback;
  final String labelText;
  final TextInputAction inputAction;
  final String initialValue;
  final FocusNode focusNode;

  const PhoneFieldIntlWidget(
      {Key key,
      this.autoValidate,
      this.onSaved,
      this.onFieldSubmitted,
      this.selectedCountry,
      this.controller,
      this.callback,
      this.labelText,
      this.inputAction,
      this.initialValue,
      this.focusNode})
      : super(key: key);

  @override
  _PhoneFieldIntlWidgetState createState() => _PhoneFieldIntlWidgetState();
}

class _PhoneFieldIntlWidgetState extends State<PhoneFieldIntlWidget> {
  Country _selectedDialogCountry = CountryPickerUtils.getCountryByIsoCode('my');
  PhoneNumber _phoneNumber = PhoneNumber();
  String errorText;

  void onSelectedPhoneCode(Country country) {
    setState(() {
      _selectedDialogCountry = country;
    });
    widget.callback(country);
  }

  @override
  void initState() {
    super.initState();
    if (widget.controller.text != null) {
      widget.controller.text =
          Validator.checkPhoneNumber(widget.controller.text);
    }
    if (widget.selectedCountry != null) {
      _selectedDialogCountry = widget.selectedCountry;
    }
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: TextFormField(
        controller: widget.controller,
        onSaved: (val) {
          var phoneNumber = Validator.checkPhoneNumber(val);
          String phone = '${_selectedDialogCountry.phoneCode}$phoneNumber';
          widget.onSaved(phone);
        },
        onChanged: (val) async {
          try {
            await _phoneNumber.parse('${_selectedDialogCountry.phoneCode}$val',
                region: _selectedDialogCountry.isoCode);
            setState(() {
              errorText = null;
            });
          } catch (e) {
            setState(() {
              errorText = Validator.validateField(val) != null
                  ? Validator.validateField(val)
                  : ErrorMessage.invalidPhoneCode;
            });
          }
        },
        onFieldSubmitted: widget.onFieldSubmitted,
        validator: (val) {
          return errorText;
        },
        keyboardType: TextInputType.phone,
        textInputAction: widget.inputAction,
        focusNode: widget.focusNode,
        initialValue: widget.initialValue != null
            ? Validator.checkPhoneNumber(widget.initialValue)
            : null,
        maxLength: 14,
        autovalidate: widget.autoValidate,
        decoration: InputDecoration(
          helperText: AppConstant.phoneNumberExample,
          errorText: errorText != null ? errorText : null,
          helperStyle: TextStyle(fontSize: AppTheme.subtitleFontSize),
          counterText: "",
          prefixIcon: GestureDetector(
            onTap: () async {
              await CountryPicker.openCountryDialog(
                  context: context,
                  callback: (val) => onSelectedPhoneCode(val));
            },
            child: FittedBox(
              child: Padding(
                padding: EdgeInsets.only(bottom: 14, top: 10, right: 4),
                child: Row(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: 4.0),
                      child: Row(
                        children: <Widget>[
                          CountryPickerUtils.getDefaultFlagImage(
                              _selectedDialogCountry),
                          SizedBox(width: 8.0),
                          Text("+${_selectedDialogCountry.phoneCode}"),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          border: UnderlineInputBorder(
            borderSide: BorderSide(color: AppTheme.primaryAppColor),
          ),
          hintText: widget.labelText,
          hintStyle: TextStyle(
              fontSize: AppTheme.subtitleFontSize,
              fontWeight: FontWeight.w400),
          icon: Icon(Icons.phone),
        ),
      ),
    );
  }
}
