import 'package:flutter/material.dart';
import 'package:myshop_flutter/src/utils/validator.dart';

class EmailFieldWidget extends StatefulWidget {
  final bool autoValidate;
  final ValueChanged<String> onSaved;
  final ValueChanged<String> onChanged;
  final ValueChanged<String> onFieldSubmitted;
  final TextEditingController controller;
  final String initialValue;
  final FocusNode focusNode;
  final String labelText;
  final TextInputAction inputAction;
  final Widget prefix;
  final bool readonly;
  final String helperText;
  final int helperMaxLines;

  const EmailFieldWidget({
    Key key,
    this.autoValidate = false,
    this.onSaved,
    this.onChanged,
    this.onFieldSubmitted,
    this.initialValue,
    this.focusNode,
    this.controller,
    this.labelText,
    this.inputAction = TextInputAction.none,
    this.prefix,
    this.readonly = false,
    this.helperText,
    this.helperMaxLines,
  }) : super(key: key);

  @override
  _EmailFieldWidgetState createState() => _EmailFieldWidgetState();
}

class _EmailFieldWidgetState extends State<EmailFieldWidget> {
  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: TextFormField(
        autovalidate: widget.autoValidate,
        onSaved: widget.onSaved,
        onChanged: widget.onChanged,
        onFieldSubmitted: widget.onFieldSubmitted,
        controller: widget.controller,
        validator: (value) {
          return Validator.validateEmail(value);
        },
        initialValue: widget.initialValue,
        focusNode: widget.focusNode,
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
          counterText: '',
          prefix: widget.prefix != null ? widget.prefix : null,
          helperText: widget.helperText != null ? widget.helperText : null,
          helperMaxLines: widget.helperMaxLines,
          labelText: widget.labelText,
          icon: Icon(Icons.email),
        ),
        textInputAction: widget.inputAction,
        readOnly: widget.readonly,
      ),
    );
  }
}
