import 'package:country_pickers/country.dart';
import 'package:country_pickers/country_pickers.dart';
import 'package:flutter/material.dart';
import 'package:myshop_flutter/src/constants/app_constant.dart';
import 'package:myshop_flutter/src/utils/validator.dart';

class PhoneFieldWidget extends StatefulWidget {
  final bool autoValidate;
  final ValueChanged<String> onSaved;
  final ValueChanged<String> onChanged;
  final ValueChanged<String> onFieldSubmitted;
  final TextEditingController controller;
  final Function callback;
  final String labelText;
  final TextInputAction inputAction;
  final String initialValue;
  final FocusNode focusNode;

  const PhoneFieldWidget(
      {Key key,
      this.autoValidate,
      this.onSaved,
      this.onChanged,
      this.onFieldSubmitted,
      this.controller,
      this.callback,
      this.labelText,
      this.inputAction,
      this.initialValue,
      this.focusNode})
      : super(key: key);

  @override
  _PhoneFieldWidgetState createState() => _PhoneFieldWidgetState();
}

class _PhoneFieldWidgetState extends State<PhoneFieldWidget> {
  Country _selectedDialogCountry = CountryPickerUtils.getCountryByIsoCode('my');

  @override
  void initState() {
    super.initState();
    if (widget.controller.text != null) {
      widget.controller.text =
          Validator.checkPhoneNumber(widget.controller.text);
    }
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: TextFormField(
        controller: widget.controller,
        onSaved: (val) {
          var phoneNumber = Validator.checkPhoneNumber(val);
          String phone = '${_selectedDialogCountry.phoneCode}$phoneNumber';
          widget.onSaved(phone);
        },
        onChanged: (val) {
          var phoneNumber = Validator.checkPhoneNumber(val);
          widget.onChanged(phoneNumber);
        },
        onFieldSubmitted: widget.onFieldSubmitted,
        validator: (val) {
          return Validator.validatePhoneCodeField(val);
        },
        keyboardType: TextInputType.phone,
        textInputAction: widget.inputAction,
        focusNode: widget.focusNode,
        initialValue: widget.initialValue != null
            ? Validator.checkPhoneNumber(widget.initialValue)
            : null,
        maxLength: 14,
        autovalidate: widget.autoValidate,
        decoration: InputDecoration(
          helperText: AppConstant.phoneNumberExample,
          helperStyle: TextStyle(fontSize: 16),
          counterText: "",
          prefixIcon: Container(
            padding: EdgeInsets.only(bottom: 14, top: 10),
            width: 80.0,
            height: 10.0,
            child: Row(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(left: 4.0),
                  child: Row(
                    children: <Widget>[
                      CountryPickerUtils.getDefaultFlagImage(
                          _selectedDialogCountry),
                      SizedBox(width: 8.0),
                      Text("+${_selectedDialogCountry.phoneCode}"),
                    ],
                  ),
                ),
              ],
            ),
          ),
          border: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.orange),
          ),
          hintText: widget.labelText,
          hintStyle: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w400),
          icon: Icon(Icons.phone),
        ),
      ),
    );
  }
}
