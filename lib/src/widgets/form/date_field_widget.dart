import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:myshop_flutter/src/utils/utils.dart';
import 'package:myshop_flutter/src/utils/validator.dart';

class DateFieldWidget extends StatefulWidget {
  final DateTime initialDate;
  final DateTime lastDate;
  final bool autoValidate;
  final TextEditingController textController;
  final String labelText;
  final IconData icon;
  final ValueChanged<String> onSaved;
  final ValueChanged<String> onChanged;
  final ValueChanged<String> onFieldSubmitted;
  final Function callback;

  const DateFieldWidget(
      {Key key,
      this.onSaved,
      this.onChanged,
      this.onFieldSubmitted,
      this.initialDate,
      this.lastDate,
      this.autoValidate,
      this.textController,
      this.labelText,
      this.callback,
      this.icon})
      : super(key: key);

  @override
  _DateFieldWidgetState createState() => _DateFieldWidgetState();
}

class _DateFieldWidgetState extends State<DateFieldWidget> {
  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: InkWell(
        onTap: () async {
          DateTime selectedDate = await Util.selectDate(
              context: context,
              pickerMode: DatePickerMode.year,
              dateState: widget.initialDate,
              lastDate: widget.lastDate);

          if (selectedDate != null) {
            String formattedDate =
                DateFormat('yyyy-MM-dd').format(selectedDate);

            setState(() {
              widget.textController.text = formattedDate;
            });
            return widget.callback(selectedDate);
          }
        },
        child: IgnorePointer(
          child: TextFormField(
            autovalidate: widget.autoValidate,
            onSaved: widget.onSaved,
            onChanged: widget.onChanged,
            onFieldSubmitted: widget.onFieldSubmitted,
            controller: widget.textController,
            validator: (value) {
              return Validator.validateField(widget.textController.text);
            },
            decoration: InputDecoration(
                labelText: widget.labelText,
                icon: Icon(widget.icon),
                counterText: ""),
            maxLength: 10,
          ),
        ),
      ),
    );
  }
}
