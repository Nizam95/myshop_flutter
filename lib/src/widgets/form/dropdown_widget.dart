import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:myshop_flutter/src/constants/messages_constant.dart';

class DropdownWidget extends StatefulWidget {
  final dynamic initialValue;
  final bool autoValidate;
  final List<dynamic> childData;
  final String helperText;
  final dynamic value;
  final Function onChange;
  final String label;
  final String hint;
  final String emptyHint;
  final IconData icon;
  final List<DropdownMenuItem<dynamic>> items;

  const DropdownWidget(
      {Key key,
      this.initialValue,
      this.autoValidate,
      this.childData,
      this.helperText,
      this.value,
      this.onChange,
      this.label,
      this.hint,
      this.emptyHint,
      this.icon,
      this.items})
      : super(key: key);

  @override
  _DropdownWidgetState createState() => _DropdownWidgetState();
}

class _DropdownWidgetState extends State<DropdownWidget> {
  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: FormField<dynamic>(
        initialValue: widget.initialValue,
        autovalidate: widget.autoValidate != null ? widget.autoValidate : false,
        builder: (FormFieldState<dynamic> state) {
          return InputDecorator(
            decoration: InputDecoration(
                icon: widget.icon != null ? Icon(widget.icon) : null,
                labelText: widget.label,
                errorText: state.hasError ? state.errorText : null,
                helperText:
                    widget.helperText != null ? widget.helperText : null),
            child: DropdownButtonHideUnderline(
              child: (widget.childData != null && widget.childData.length > 0)
                  ? DropdownButton<dynamic>(
                      hint: Text(widget.hint),
                      value: widget.value,
                      isDense: true,
                      isExpanded: true,
                      onChanged: (dynamic value) {
                        widget.onChange(value);
                        state.didChange(value);
                      },
                      items: widget.items,
                    )
                  : DropdownButton(
                      isDense: true,
                      items: null,
                      onChanged: null,
                      hint: AutoSizeText(
                        widget.emptyHint,
                      ),
                      isExpanded: true,
                    ),
            ),
          );
        },
        validator: (val) {
          return widget.value != null ? null : ErrorMessage.generalFieldError;
        },
      ),
    );
  }
}
