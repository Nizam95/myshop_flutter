import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class TextFormFieldWidget extends StatefulWidget {
  final autoValidate;
  final Function validator;
  final FormFieldSetter<String> onSaved;
  final ValueChanged<String> onChanged;
  final Function onFieldSubmitted;
  final TextEditingController controller;
  final String initialValue;
  final FocusNode focusNode;
  final String labelText;
  final IconData icon;
  final TextInputType inputType;
  final TextInputAction inputAction;
  final int maxLength;
  final Widget prefix;
  final Widget suffixIcon;
  final bool readonly;
  final String errorText;
  final String helperText;
  final int helperMaxLines;
  final List<TextInputFormatter> inputFormatter;

  const TextFormFieldWidget({
    Key key,
    this.autoValidate,
    this.validator,
    this.onSaved,
    this.onChanged,
    this.onFieldSubmitted,
    this.initialValue,
    this.focusNode,
    this.controller,
    this.labelText,
    this.icon,
    this.inputType,
    this.inputAction = TextInputAction.none,
    this.maxLength,
    this.prefix,
    this.suffixIcon,
    this.readonly = false,
    this.errorText,
    this.helperText,
    this.helperMaxLines,
    this.inputFormatter,
  }) : super(key: key);

  @override
  _TextFormFieldWidgetState createState() => _TextFormFieldWidgetState();
}

class _TextFormFieldWidgetState extends State<TextFormFieldWidget> {
  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: TextFormField(
        autovalidate: widget.autoValidate,
        onSaved: widget.onSaved,
        onChanged: widget.onChanged,
        onFieldSubmitted: widget.onFieldSubmitted,
        controller: widget.controller,
        validator: widget.validator != null
            ? widget.validator
            : (value) {
          if (value.isEmpty) {
            return widget.errorText != null
                ? widget.errorText
                : FlutterI18n.translate(context, "error_msg.fieldCannotEmpty");
          }
          return null;
        },
        inputFormatters: widget.inputFormatter,
        initialValue: widget.initialValue,
        focusNode: widget.focusNode,
        keyboardType: widget.inputType,
        maxLength: widget.maxLength,
        decoration: InputDecoration(
          counterText: '',
          prefix: widget.prefix != null ? widget.prefix : null,
          helperText: widget.helperText != null ? widget.helperText : null,
          suffixIcon: widget.suffixIcon != null ? widget.suffixIcon : null,
          helperMaxLines: widget.helperMaxLines,
          labelText: widget.labelText,
          icon: widget.icon != null ? Icon(widget.icon) : null,
        ),
        textInputAction: widget.inputAction,
        readOnly: widget.readonly,
      ),
    );
  }
}
