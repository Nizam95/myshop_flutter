import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:myshop_flutter/src/constants/app_theme_constant.dart';
import 'package:myshop_flutter/src/utils/validator.dart';

class BorderTextFieldWidget extends StatelessWidget {
  final String labelText;
  final String hintText;
  final String prefixText;
  final TextInputType textInputType;
  final IconData prefixIcon;
  final TextEditingController controller;
  final bool autoValidate;
  final ValueChanged<String> onChanged;
  final String initialValue;
  final FocusNode focusNode;
  final TextInputType inputType;
  final int maxLength;
  final Widget suffixIcon;
  final bool readonly;
  final bool obscureText;
  final String helperText;
  final int helperMaxLines;
  final List<TextInputFormatter> inputFormatter;
  final Function validator;

  const BorderTextFieldWidget({
    Key key,
    this.labelText,
    this.hintText,
    this.prefixText,
    this.textInputType,
    this.prefixIcon,
    this.controller,
    this.onChanged,
    this.initialValue,
    this.focusNode,
    this.inputType,
    this.maxLength,
    this.suffixIcon,
    this.readonly = false,
    this.obscureText,
    this.helperText,
    this.helperMaxLines,
    this.inputFormatter,
    this.validator,
    this.autoValidate,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          labelText,
          style: AppTheme.kLabelStyle,
        ),
        AppTheme.dividerVerticalSmall,
        Material(
          elevation: AppTheme.elevationMax,
          color: Colors.transparent,
          shadowColor: Colors.black12,
          child: TextFormField(
            obscureText: obscureText != null ? obscureText : false,
            onChanged: onChanged,
            inputFormatters: inputFormatter,
            initialValue: initialValue,
            focusNode: focusNode,
            maxLength: maxLength,
            readOnly: readonly,
            validator: validator != null
                ? validator
                : (value) {
                    return Validator.validateField(value);
                  },
            keyboardType:
                textInputType != null ? textInputType : TextInputType.text,
            controller: controller,
            style: TextStyle(
              color: AppTheme.white,
            ),
            decoration: InputDecoration(
              filled: true,
              fillColor: AppTheme.primaryAppColor.withOpacity(0.8),
              border: AppTheme.outlineTextInputBorderStyle,
              enabledBorder: AppTheme.outlineTextInputBorderStyle,
              focusedBorder: OutlineInputBorder(
                borderRadius: AppTheme.borderRadiusMedium,
                borderSide: BorderSide(
                  color: AppTheme.primaryAppColor,
                  width: 1.0,
                ),
              ),
              focusedErrorBorder: AppTheme.outlineTextInputBorderStyle,
              errorBorder: AppTheme.outlineTextInputErrorBorderStyle,
              counterText: '',
              suffixIcon: obscureText != null ? suffixIcon : null,
              contentPadding: EdgeInsets.all(AppTheme.paddingVerticalBig + 2),
              prefixIcon: Padding(
                padding: EdgeInsets.only(
                    left: AppTheme.paddingHorizontalBig,
                    right: AppTheme.paddingHorizontalBig - 6,
                    top: AppTheme.paddingVerticalExtraSmall - 2),
                child: prefixText != null
                    ? Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Container(
                            child: Row(
                              children: <Widget>[
                                Icon(
                                  prefixIcon,
                                  color: AppTheme.white,
                                ),
                                SizedBox(width: 8.0),
                                Text(
                                  prefixText,
                                  style: TextStyle(
                                      color: AppTheme.white,
                                      fontWeight: FontWeight.w500,
                                      fontSize: AppTheme.subtitleFontSize),
                                ),
                              ],
                            ),
                          ),
                        ],
                      )
                    : Icon(
                        prefixIcon,
                        color: AppTheme.white,
                      ),
              ),
              hintText: hintText,
              hintStyle: AppTheme.kHintTextStyle,
            ),
          ),
        ),
      ],
    );
  }
}
