import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:myshop_flutter/src/constants/enum_constant.dart';
import 'package:myshop_flutter/src/constants/app_theme_constant.dart';

class TextFieldWidget extends StatelessWidget {
  final String label;
  final bool isSecuredText;
  final TextInputType textInputType;
  final TextEditingController controller;
  final Function validator;
  final void Function(String) onChanged;
  final VoidCallback suffixOnTap;
  final double fontSize;
  final String hint;
  final bool suffixVisibility;
  final InputBorder inputBorder;
  final bool enable;
  final String errorText;
  final int maxLength;
  final Widget prefix;
  final Widget suffix;
  final List<TextInputFormatter> inputFormatter;
  final TextCapitalization textCapitalization;
  final bool readonly;
  final SuffixType suffixType;

  const TextFieldWidget(
      {Key key,
      this.label,
      this.isSecuredText = false,
      this.textInputType = TextInputType.text,
      this.controller,
      this.validator,
      this.onChanged,
      this.suffixOnTap,
      this.fontSize,
      this.hint,
      this.suffixVisibility = false,
      this.inputBorder,
      this.enable = true,
      this.errorText,
      this.maxLength,
      this.prefix,
      this.suffix,
      this.inputFormatter,
      this.textCapitalization,
      this.readonly = false,
      this.suffixType})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: AppTheme.paddingHorizontalBig,
          vertical: AppTheme.paddingVerticalExtraSmall),
      child: TextFormField(
        controller: controller,
        validator: validator != null
            ? validator
            : (value) {
                if (value.isEmpty) {
                  return errorText;
                }
                return null;
              },
        onChanged: onChanged,
        readOnly: readonly,
        textCapitalization: textCapitalization != null
            ? textCapitalization
            : TextCapitalization.none,
        inputFormatters: inputFormatter,
        keyboardType: textInputType,
        enabled: enable,
        enableInteractiveSelection: enable,
        autofocus: false,
        maxLength: maxLength,
        obscureText: isSecuredText,
        style: TextStyle(
            color: AppTheme.primaryAppColor, fontWeight: FontWeight.w800),
        decoration: InputDecoration(
          floatingLabelBehavior: FloatingLabelBehavior.always,
          counterText: '',
          border: inputBorder,
          hintStyle: TextStyle(
              color: AppTheme.primaryAppColor,
              fontWeight: FontWeight.w800),
          isDense: true,
          hintText: (hint != null) ? hint : label,
          prefix: prefix != null ? prefix : null,
          labelText: label,

          suffixIcon: suffixVisibility ? buildSuffixTypeWidget() : null,
//          contentPadding: EdgeInsets.only(
//            top: AppTheme.paddingVerticalMedium,
//          ),
        ),
      ),
    );
  }

  Widget buildSuffixTypeWidget() {
    if (this.suffixType == SuffixType.scan) {
      return GestureDetector(
          child: Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
            Text("SCAN",
                style: TextStyle(
                    color: AppTheme.primaryDarkColor,
                    fontSize: AppTheme.captionFontSize,
                    fontWeight: FontWeight.bold)),
            Container(
                child: Image.asset("assets/images/icons/barcode_icon.png",
                    color: AppTheme.primaryDarkColor,
                    width: 20.0,
                    height: 20.0),
                margin: EdgeInsets.only(left: 10.0))
          ]),
          onTap: suffixOnTap);
    } else if (this.suffixType == SuffixType.visibility) {
      return GestureDetector(
          child: Icon(isSecuredText ? Icons.visibility : Icons.visibility_off),
          onTap: suffixOnTap);
    } else
      return null;
  }
}
