import 'package:flutter/material.dart';
import 'package:flutter_beautiful_popup/main.dart';
import 'package:flutter_beautiful_popup/templates/Common.dart';
import 'package:get/get.dart';
import 'package:myshop_flutter/src/constants/assets_path.dart';

class TemplateComingSoon extends BeautifulPopupTemplate {
  final BeautifulPopup options;
  TemplateComingSoon(this.options) : super(options);

  @override
  final illustrationKey = AssetsPath.NotificationBg;
  @override
  Color get primaryColor => Get.theme.primaryColor;
  @override
  final maxWidth = 350;
  @override
  final maxHeight = 400;
  @override
  final bodyMargin = 20;
  @override
  get layout {
    return [
      Positioned(
        child: Image.asset(
          illustrationKey,
          width: percentW(100),
          height: percentH(100),
          fit: BoxFit.fill,
        ),
      ),
      Positioned(
        top: percentH(50),
        child: Container(
          width: percentW(100),
          height: percentH(20),
          child: Opacity(
            opacity: 0.95,
            child: Text(
              options.title,
              style: TextStyle(
                fontSize:
                    Theme.of(options.context).textTheme.headline4.fontSize,
                color: primaryColor,
                fontWeight: FontWeight.bold,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ),
      Positioned(
        bottom: percentW(8),
        left: percentW(8),
        right: percentW(8),
        child: actions ?? Container(),
      ),
    ];
  }
}
