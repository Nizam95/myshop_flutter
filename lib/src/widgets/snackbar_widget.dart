import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SnackbarWidget {
  static void showSuccess(String message) {
    Get.rawSnackbar(
      title: 'Success',
      icon: Icon(
        Icons.thumb_up,
        color: Colors.white,
      ),
      message: message,
      backgroundColor: Colors.green[600],
      instantInit: true,
    );
  }

  static void showCustom({
    String message,
    String title,
    bool isDismissible = true,
    SnackStyle snackStyle = SnackStyle.FLOATING,
    SnackPosition snackPosition = SnackPosition.TOP,
    Duration duration,
    Color backgroundColor,
    Color leftBarIndicatorColor,
    Widget titleText,
    Widget messageText,
    Widget icon,
    bool instantInit = true,
    Gradient backgroundGradient,
    FlatButton mainButton,
    OnTap onTap,
    SnackDismissDirection dismissDirection = SnackDismissDirection.VERTICAL,
    bool showProgressIndicator = false,
    AnimationController progressIndicatorController,
    Color progressIndicatorBackgroundColor,
    Animation<Color> progressIndicatorValueColor,
    Curve forwardAnimationCurve = Curves.easeOutCirc,
    Curve reverseAnimationCurve = Curves.easeOutCirc,
    Duration animationDuration = const Duration(seconds: 1),
    SnackStatusCallback onStatusChanged,
    Color overlayColor = Colors.transparent,
  }) {
    Get.rawSnackbar(
      title: title,
      snackPosition: snackPosition,
      duration: duration,
      icon: Icon(
        Icons.info,
        color: Colors.white,
      ),
      message: message,
      isDismissible: isDismissible,
      snackStyle: snackStyle,
      backgroundColor:
          backgroundColor != null ? backgroundColor : Colors.green[600],
      instantInit: true,
      leftBarIndicatorColor: leftBarIndicatorColor,
      onTap: onTap,
      backgroundGradient: backgroundGradient,
      mainButton: mainButton,
      dismissDirection: dismissDirection,
      showProgressIndicator: showProgressIndicator,
      progressIndicatorController: progressIndicatorController,
      progressIndicatorBackgroundColor: progressIndicatorBackgroundColor,
      progressIndicatorValueColor: progressIndicatorValueColor,
      forwardAnimationCurve: forwardAnimationCurve,
      reverseAnimationCurve: reverseAnimationCurve,
      animationDuration: animationDuration,
      onStatusChanged: onStatusChanged,
    );
  }

  static void showWarning(String message) {
    Get.rawSnackbar(
      title: 'Warning',
      icon: Icon(
        Icons.warning,
        color: Colors.white,
      ),
      message: message,
      backgroundColor: Colors.orange[900],
      instantInit: true,
    );
  }

  static void showComingSoon(String message) {
    Get.rawSnackbar(
      title: 'Coming Soon',
      icon: Icon(
        Icons.info,
        color: Colors.white,
      ),
      message: message,
      backgroundColor: Colors.orange[500],
      instantInit: true,
    );
  }

  static void showError(String message) {
    Get.rawSnackbar(
      title: 'Error',
      icon: Icon(
        Icons.error,
        color: Colors.white,
      ),
      message: message,
      backgroundColor: Colors.redAccent[700],
      instantInit: true,
    );
  }
}
