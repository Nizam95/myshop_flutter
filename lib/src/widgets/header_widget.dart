import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myshop_flutter/src/constants/app_theme_constant.dart';

class HeaderWidget extends StatelessWidget {
  final String iconImage;
  final String title;

  const HeaderWidget({Key key, this.iconImage, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Image.asset(iconImage, width: 100, height: 100),
          AppTheme.dividerVerticalSmall,
          Text(
            title,
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Colors.black, fontSize: AppTheme.bigTitleFontSize),
          ),
          AppTheme.dividerVerticalBig,
          Divider(
            color: Get.theme.primaryColor,
            thickness: 2,
            indent: 40,
            endIndent: 40,
          ),
        ],
      ),
    );
  }
}
