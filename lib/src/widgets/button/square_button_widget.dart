import 'package:flutter/material.dart';
import 'package:myshop_flutter/src/constants/app_theme_constant.dart';

class SquareButtonWidget extends StatelessWidget {
  final VoidCallback callback;
  final String btnText;
  final Color btnColor;
  final Color textColor;
  final double fontSize;
  final EdgeInsetsGeometry padding;
  final ShapeBorder shape;

  const SquareButtonWidget({
    Key key,
    this.callback,
    this.btnText,
    this.btnColor,
    this.textColor,
    this.fontSize,
    this.padding,
    this.shape,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      color: btnColor,
      child: Text(
        btnText,
        style: TextStyle(
            fontSize: fontSize != null ? fontSize : AppTheme.titleFontSize,
            color: textColor),
      ),
      onPressed: callback,
      elevation: 4,
      padding: padding != null ? padding : AppTheme.paddingMedium,
      shape: shape!= null ? shape : AppTheme.roundBorderExtraSmall,
    );
  }
}
