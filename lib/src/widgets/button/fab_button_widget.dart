import 'package:flutter/material.dart';
import 'package:myshop_flutter/src/constants/app_theme_constant.dart';

class FabButtonWidget extends StatelessWidget {
  final Function onPressed;

  const FabButtonWidget({Key key, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topLeft,
      child: Padding(
        padding: AppTheme.fabPadding,
        child: FloatingActionButton(
          mini: true,
          foregroundColor: Colors.black54,
          backgroundColor: Colors.white,
          elevation: AppTheme.elevationBig,
          child: Center(
            child: Icon(Icons.arrow_back_ios, size: AppTheme.fabIconSizeBig),
          ),
          onPressed: onPressed,
        ),
      ),
    );
  }
}
