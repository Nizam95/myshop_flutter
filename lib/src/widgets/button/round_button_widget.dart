import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myshop_flutter/src/constants/app_theme_constant.dart';

class RoundButtonWidget extends StatelessWidget {
  final VoidCallback callback;
  final String btnText;

  const RoundButtonWidget({Key key, this.callback, this.btnText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
          vertical: AppTheme.paddingHorizontalExtraBig),
      width: Get.width,
      child: RaisedButton(
        elevation: AppTheme.elevationExtraBig,
        onPressed: callback,
        padding: AppTheme.paddingBig,
        shape: AppTheme.roundBorderExtraBig,
        color: AppTheme.white,
        child: Text(btnText, style: AppTheme.buttonTextStyle),
      ),
    );
  }
}
