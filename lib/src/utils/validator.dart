import 'package:myshop_flutter/src/constants/messages_constant.dart';
import 'package:myshop_flutter/src/utils/utils.dart';
import 'package:phone_number/phone_number.dart';

class Validator {
  static String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (value.isEmpty) {
      return ErrorMessage.fieldCannotEmpty;
    } else if (!regex.hasMatch(value))
      return ErrorMessage.invalidEmail;
    else
      return null;
  }

  static String validateField(String value) {
    if (value.isEmpty) {
      return ErrorMessage.fieldCannotEmpty;
    } else
      return null;
  }

  static String validateFieldWithLength(
      {String label, String value, int minLength, int maxLength}) {
    if (value.isEmpty) {
      return ErrorMessage.fieldCannotEmpty;
    } else if (minLength != null && value.length < minLength) {
      return "$label length must be more than $minLength";
    } else if (maxLength != null && value.length > maxLength) {
      return "$label length must be less than or equal to $maxLength";
    } else
      return null;
  }

  static String validateICNumber(String value) {
    if (value.isEmpty) {
      return 'Please Enter your IC Number';
    } else {
      String ic = value.replaceAll('-', '');
      if (ic.length != 12) {
        return 'Invalid IC Number. Please provide a valid IC Number';
      } else
        return null;
    }
  }

  static String validateFieldNumeric(String value) {
    if (value.isEmpty) {
      return ErrorMessage.generalFieldError;
    }
    if (!Util().isNumeric(value)) {
      return ErrorMessage.generalNumericFieldError;
    } else
      return null;
  }

  static String validateAddress(String value) {
    if (value.isEmpty) {
      return ErrorMessage.postcodeError;
    } else
      return null;
  }

  static String validateDropdown(String value) {
    if (value == null || value.isEmpty) {
      return ErrorMessage.fieldCannotEmpty;
    } else
      return null;
  }

  static String validatePhoneCodeField(String value) {
    if (value.isEmpty) {
      return ErrorMessage.fieldCannotEmpty;
    }
    //    value.startsWith("0") ||
    else if (value.length < 9 || value.length > 16)
      return ErrorMessage.invalidPhoneNumber;
    else
      return null;
  }

  static Future<String> validatePhoneCodeIntlField(
      String value, String region) async {
    if (value.isEmpty) {
      return ErrorMessage.fieldCannotEmpty;
    } else {
      return PhoneNumber()
          .parse(value, region: region)
          .then((val) => null)
          .catchError((error) => ErrorMessage.invalidPhoneNumber);
    }
  }

  static String checkPhoneNumber(String value) {
    if (value.startsWith("0")) {
      return value.substring(1);
    } else if (value.startsWith("6")) {
      return value.substring(2);
    } else
      return value;
  }

  static String removePhoneCode(int length, String value) {
    return value.substring(length);
  }

  static String validatePhoneField(String value) {
    if (value.isEmpty) {
      return ErrorMessage.fieldCannotEmpty;
    } else if (value.length < 9 || value.length > 16)
      return ErrorMessage.invalidPhoneNumber;
    else
      return null;
  }

  static String validatePasswordLoginField(String value) {
    return null;
  }

  static String validatePasswordField(String value) {
    // var hasNumber = new RegExp("[0-9]+");
    // var hasUpperChar = new RegExp("[A-Z]+");
    // var hasLowerChar = new RegExp("[a-z]+");
    // var hasSymbols = new RegExp("[!@#\$%^&*()_+=\[{\]};:<>|./?,-]");

    if (value.isEmpty)
      return ErrorMessage.fieldCannotEmpty;
    else if (value.length < 5) return ErrorMessage.passwordLengthError;
//    else if (!hasNumber.hasMatch(value))
//      return 'Password must at least contain a number.';
    return null;
  }

  static String validateConfirmPasswordField(
      String value, String confirmValue) {
    if (confirmValue.isEmpty)
      return ErrorMessage.fieldCannotEmpty;
    else if (value != confirmValue) return ErrorMessage.passwordNotMatch;
    return null;
  }
}
