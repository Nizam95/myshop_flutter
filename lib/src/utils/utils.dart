import 'dart:math';
import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Util {
  static String convertToSha256(List<int> bytes) {
    String hash = sha256.convert(bytes).toString();
    return hash;
  }

  bool isNumeric(String s) {
    if (s == null) {
      return false;
    }
    return double.tryParse(s) != null;
  }

  randomNumber() {
    var rng = new Random();
    for (var i = 0; i < 4; i++) {
      print(rng.nextInt(100));
    }
  }

  bool equalsIgnoreCase(String string1, String string2) {
    return string1?.toLowerCase() == string2?.toLowerCase();
  }

  bool checkNullOrEmpty(String string) {
    if (string != null && string != '') {
      return false;
    } else
      return true;
  }

  bool validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = RegExp(pattern);
    if (!regex.hasMatch(value))
      return true;
    else
      return false;
  }

  String formatDate(String datetime, String dateFormat) {
    try {
      DateTime parsedDate = DateTime.parse(datetime);
      var format = DateFormat(dateFormat);
      return format.format(parsedDate);
    } catch (e) {
      return '';
    }
  }

  dynamic checkExistValue({dynamic defaultValue, dynamic value}) {
    if (value != null)
      return value;
    else
      return defaultValue;
  }

  static String enumName(String enumToString) {
    List<String> paths = enumToString.split(".");
    return paths[paths.length - 1];
  }

  String sentenceCase(String text) {
    var splitStr = text.toLowerCase().split(' ');
    for (var i = 0; i < splitStr.length; i++) {
      splitStr[i] = splitStr[i][0].toUpperCase() + splitStr[i].substring(1);
    }
    return splitStr.join(' ');
  }

  static Future<DateTime> selectDate(
      {BuildContext context,
      DateTime dateState,
      lastDate,
      DatePickerMode pickerMode}) async {
    final DateTime picked = await showDatePicker(
        initialDatePickerMode: pickerMode,
        context: context,
        initialDate: dateState,
        firstDate: DateTime(1950),
        lastDate: lastDate);
    return picked;
  }
}
