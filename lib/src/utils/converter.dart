import 'package:flutter/cupertino.dart';

class Converter {
  static TextInputType parseTextFieldInputType(String inputType) {
    if (inputType != null) {
      if (inputType == "number") {
        return TextInputType.numberWithOptions(decimal: true);
      } else if (inputType == "phone") {
        return TextInputType.numberWithOptions(decimal: false);
      } else if (inputType == "email") {
        return TextInputType.emailAddress;
      } else
        return TextInputType.text;
    } else
      return TextInputType.text;
  }
}
