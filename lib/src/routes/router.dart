import 'package:get/get.dart';
import 'package:myshop_flutter/src/constants/route_path.dart';
import 'package:myshop_flutter/src/navigation/main_navigation.dart';
import 'package:myshop_flutter/src/views/common/web_view.dart';
import 'package:myshop_flutter/src/views/login_view.dart';
import 'package:myshop_flutter/src/views/register_view.dart';
import 'package:myshop_flutter/src/views/side/change-password/change_password_view.dart';
import 'package:myshop_flutter/src/views/side/notification/notification_view.dart';
import 'package:myshop_flutter/src/views/side/profile/profile_view.dart';
import 'package:myshop_flutter/src/views/splash_screen_view.dart';
import 'package:myshop_flutter/src/views/verify_phone_view.dart';

class Router {
  static List<GetPage> routesSetting = [
    ///MAIN
    GetPage(
      name: RoutePaths.MainPage,
      transition: Transition.topLevel,
      page: () => MainNavigation(),
    ),

    ///SPLASH
    GetPage(
      name: RoutePaths.SplashScreenPage,
      transition: Transition.topLevel,
      page: () => SplashScreenPage(),
    ),

    ///PUBLIC
    GetPage(
        name: RoutePaths.LoginPage,
        transition: Transition.topLevel,
        page: () => LoginPage()),
    GetPage(
      name: RoutePaths.VerifyPhonePage,
      transition: Transition.topLevel,
      page: () => VerifyPhonePage(),
    ),
    GetPage(
      name: RoutePaths.RegisterPage,
      transition: Transition.topLevel,
      page: () => RegisterPage(),
    ),

    ///COMMON
    GetPage(
      name: RoutePaths.WebPageViewer,
      transition: Transition.topLevel,
      page: () => WebPageViewer(),
    ),

    ///DRAWER
    GetPage(
        name: RoutePaths.NotificationPage,
        transition: Transition.topLevel,
        page: () => NotificationPage()),
    GetPage(
        name: RoutePaths.ChangePasswordPage,
        transition: Transition.topLevel,
        page: () => ChangePasswordPage()),
    GetPage(
        name: RoutePaths.ProfilePage,
        transition: Transition.topLevel,
        page: () => ProfilePage()),
  ];
}
