import 'app_constant.dart';

class SuccessMessage {
  SuccessMessage._();

  static const String verifyPhoneSuccess =
      "Your phone number successfully verified.";
  static const String verifyIdentitySuccess =
      "Your identity is successfully verified.";
  static const String registrationSuccess = "Registration success.";
  static const String changePasswordSuccess = "Password successfully updated.";
}

class InfoMessage {
  InfoMessage._();
  static const String numberNotRegisteredNote =
      "This number is not registered with ${AppConstant.AppName}. Do you want to send a message with an invitation to ${AppConstant.AppName} to this number.";
  static const String shareMessage =
      "Check out this amazing app : ${UrlConstant.PlayStoreURL}";
  static const String shareSubject = "Amazing App";
}

class ErrorMessage {
  ErrorMessage._();

  ///FIELD ERROR
  static const String generalFieldError =
      'Please fill in all required information.';
  static const String generalNumericFieldError = "Only number allowed.";
  static const String fieldCannotEmpty = "Field cannot be empty";
  static const String invalidEmail = "Invalid email address";
  static const String invalidPhoneNumber = "Invalid Phone Number";
  static const String passwordLengthError =
      "Password length must be minimum 6 character.";
  static const String passwordNotMatch = "Password not match";
  static const String pleaseCheckConfirmationBox =
      "Please check the confirmation box.";
  static const String pleaseUploadReceipt = "Please upload receipt.";

  ///GENERAL API ERROR
  static const String generalError = 'Something wrong. Please try again later.';
  static const String verifyPhoneError =
      "Could not verify your phone number. Please try again.";
  static const String errorSendVerificationCode =
      "Failed to send verification code. Please Try Again.";

  ///VERIFICATION ERROR
  static const String unableToCheckLatestVersion =
      "Unable to check for latest version!";
  static const String phoneVerificationError =
      'Could not verify your phone number. Please try again.';
  static const String invalidPhoneCode = "Invalid phone number";
  static const String postcodeError = 'Please provide a valid postcode.';

  ///NOT AVAILABLE
  static const String featureNotAvailableError =
      'This feature will be available soon. Stay tuned.';
}
