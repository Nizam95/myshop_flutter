import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myshop_flutter/src/constants/app_constant.dart';

class AppTheme {
  ///this is to prevent anyone from instantiating this object
  AppTheme._();
  static const double marginHorizontalSmall = 8;
  static const double marginHorizontalBig = 16;

  static const double marginVerticalSmall = 8;
  static const double marginVerticalBig = 16;
  static const double paddingHorizontalSmall = 8;
  static const double paddingHorizontalMedium = 12;
  static const double paddingHorizontalBig = 16;
  static const double paddingHorizontalMediumBig = 20;
  static const double paddingHorizontalExtraBig = 24;
  static const double paddingVerticalExtraSmall = 4;
  static const double paddingVerticalSmall = 8;
  static const double paddingVerticalMedium = 12;
  static const double paddingVerticalBig = 16;
  static const double paddingVerticalExtraBig = 24;
  static const double padding40 = 40.0;

  static const double elevationSmall = 1.0;
  static const double elevationBig = 3.0;
  static const double elevationExtraBig = 6.0;
  static const double elevationMax = 12;

  static const double fabIconSizeBig = 18.0;

  static double titleFontSize = Get.textTheme.headline6.fontSize;
  static double smallTitleFontSize = Get.textTheme.headline6.fontSize - 2;
  static double bigTitleFontSize = Get.textTheme.headline5.fontSize;
  static double bodyFontSize = Get.textTheme.bodyText1.fontSize;
  static double subtitleFontSize = Get.textTheme.subtitle1.fontSize;
  static double headlineFontSize = Get.textTheme.headline4.fontSize;
  static double captionFontSize = Get.textTheme.caption.fontSize;

  static const Color white = Colors.white;
  static const Color whiteGrey = Color.fromARGB(255, 240, 240, 240);
  static const Color grey = Colors.grey;
  static const Color primaryAppColor = Color(0xffd23851);
  static const Color primaryDarkColor = Color(0xfff6f6f94);
  static const Color textPrimaryColor = Color(0xffd5465e);
  static const Color accentAppColor = Color(0xffd8546a);
  static const Color accentDarkColor = Color(0xffb92a42);

  static const List<Color> backgroundGradientColor = [
    Color(0xffd23851),
    Color(0xff9A2337),
    Color(0xff6C1926),
  ];

  static Gradient primaryGradient = LinearGradient(
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter,
      colors: backgroundGradientColor,
      stops: [0.1, 0.5, 0.9]);

  static Color primaryColor = Get.theme.primaryColor;
  static final BoxDecoration gradientBackground =
      BoxDecoration(gradient: primaryGradient);

  static final OutlineInputBorder outlineInputBorderInactiveSmall =
      OutlineInputBorder(
          borderRadius: BorderRadius.circular(8.0),
          borderSide: BorderSide(color: grey, width: 1.0));
  static final OutlineInputBorder outlineInputBorderActiveSmall =
      OutlineInputBorder(
    borderRadius: BorderRadius.circular(8.0),
    borderSide: BorderSide(
      color: primaryAppColor,
      width: 2.0,
    ),
  );

  static final BorderRadius borderRadiusSmall = BorderRadius.circular(8);
  static final BorderRadius borderRadiusMedium = BorderRadius.circular(10);

  static final ShapeBorder roundBorderExtraSmall =
      RoundedRectangleBorder(borderRadius: BorderRadius.circular(4));
  static final ShapeBorder roundBorderSmall =
      RoundedRectangleBorder(borderRadius: BorderRadius.circular(8));
  static final ShapeBorder roundBorderBig =
      RoundedRectangleBorder(borderRadius: BorderRadius.circular(16));
  static final ShapeBorder roundBorderExtraBig = RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(30.0),
  );

  static final Widget dividerVerticalExtraSmall = SizedBox(height: 4);
  static final Widget dividerVerticalSmall = SizedBox(height: 8);
  static final Widget dividerVerticalBig = SizedBox(height: 16);
  static final Widget dividerVerticalExtraBig = SizedBox(height: 24);
  static final Widget dividerHorizontalSmall = SizedBox(width: 8);
  static final Widget dividerHorizontalBig = SizedBox(width: 16);
  static final Widget dividerHorizontalExtraBig = SizedBox(width: 24);

  static final Widget sidebarDivider = Divider(
    color: Colors.grey,
    thickness: 1,
    indent: 20,
    endIndent: 20,
  );

  static final Widget defaultAppBar = AppBar(
    title: FittedBox(
        fit: BoxFit.fitWidth, child: Text(AppConstant.AppName.toUpperCase())),
    backgroundColor: AppTheme.accentAppColor,
    elevation: elevationBig,
    centerTitle: true,
    actions: <Widget>[],
  );

  static final EdgeInsetsGeometry containerPadding = EdgeInsets.only(
    left: 40.0,
    right: 40.0,
    top: 120.0,
  );
  static final fabPadding = EdgeInsets.only(top: 40, left: 20);
  static final bigBtnPadding =
      EdgeInsets.only(left: 40.0, right: 40.0, bottom: 40.0);

  static final EdgeInsetsGeometry marginSmall = EdgeInsets.all(8);
  static final EdgeInsetsGeometry marginBig = EdgeInsets.all(16);
  static final EdgeInsetsGeometry paddingSmall = EdgeInsets.all(8);
  static final EdgeInsetsGeometry paddingExtraSmall = EdgeInsets.all(4);
  static final EdgeInsetsGeometry paddingMedium = EdgeInsets.all(12);
  static final EdgeInsetsGeometry paddingBig = EdgeInsets.all(16);
  static final EdgeInsetsGeometry paddingExtraBig = EdgeInsets.all(24);

  static Widget bodyIcon({String asset, double width, double height}) {
    return Image.asset(
      asset,
      width: width != null ? width : Get.width / 3,
      height: height != null ? height : Get.width / 3,
    );
  }

  static final TextStyle titleTextStyle = TextStyle(
    color: Colors.white,
    fontSize: titleFontSize,
    fontWeight: FontWeight.bold,
  );

  static final kHintTextStyle = TextStyle(
    color: Colors.white70,
  );
  static final kLabelStyle = TextStyle(
    color: Colors.white,
    fontWeight: FontWeight.bold,
  );
  static final buttonTextStyle = TextStyle(
    color: primaryAppColor,
    letterSpacing: 1.5,
    fontSize: 18.0,
    fontWeight: FontWeight.bold,
  );

  static final outlineTextInputBorderStyle = OutlineInputBorder(
    borderRadius: BorderRadius.circular(10.0),
    borderSide: BorderSide(
      color: primaryAppColor,
      width: 1.0,
    ),
  );

  static final outlineTextInputErrorBorderStyle = OutlineInputBorder(
    borderRadius: BorderRadius.circular(10.0),
    borderSide: BorderSide(
      color: Colors.redAccent,
      width: 1.0,
    ),
  );

  static final kBoxDecorationStyle = BoxDecoration(
    color: Color(0xFFe44553).withOpacity(0.8),
    borderRadius: BorderRadius.circular(10.0),
    boxShadow: [
      BoxShadow(
        color: Colors.black12,
        blurRadius: 6.0,
        offset: Offset(0, 2),
      ),
    ],
  );
}
