/// Add your new productFlavors define in android/app/build.gradle here
enum Flavor { dev, staging, production }

enum DialogAction { yes, abort, success }
enum YesNo { yes, no }
enum SuffixType { scan, visibility }
enum ImageStatus { Camera, Gallery }
