import 'package:catcher/handlers/console_handler.dart';
import 'package:catcher/handlers/email_manual_handler.dart';
import 'package:catcher/mode/dialog_report_mode.dart';
import 'package:catcher/model/catcher_options.dart';

class AppConstant {
  AppConstant._();
  static const String AppName = 'MYSHOP';
  static const String Home = 'Home';
  static const String Tab1 = 'Tab 1';
  static const String Tab2 = 'Tab 2';
  static const String Confirmation = 'Confirmation';
  static const String Notification = 'Notification';
  static const String ChangePassword = 'Change Password';
  static const String Profile = 'Profile';
  static const String phoneNumberExample = "eg: 123456789";

  static const String AppId = "com.app.myshop";
}

class UrlConstant {
  UrlConstant._();
  static const String PlayStoreURL =
      "https://play.google.com/store/apps/details?id=${AppConstant.AppId}";
}

class StorageKey {
  StorageKey._();
  static const String UserData = 'user-data';
  static const String IsFirstTime = 'isFirstTime';
  static const String LanguageData = "language";
}

class CatcherConfig {
  CatcherConfig._();

  static final CatcherOptions debugOptions =
      CatcherOptions(DialogReportMode(), [ConsoleHandler()]);
  static final CatcherOptions releaseOptions =
      CatcherOptions(DialogReportMode(), [
    EmailManualHandler(["myshop.dev@gmail.com"],
        enableDeviceParameters: true,
        enableStackTrace: true,
        enableCustomParameters: true,
        enableApplicationParameters: true,
        sendHtml: true,
        emailTitle: "Myshop App Error Report",
        printLogs: true)
  ]);
}
