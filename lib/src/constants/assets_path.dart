class AssetsPath {
  AssetsPath._();

  ///ICON
  static const String ShopIcon = 'assets/images/icons/shop-icon.png';
  static const String Notification = 'assets/images/icons/notification.png';
  static const String Lock = 'assets/images/icons/lock.png';
  static const String Person = 'assets/images/icons/person.png';

  ///LOGO
  static const String AppLogo = 'assets/images/icons/app-logo.png';

  ///BACKGROUND
//  static const String LoginBg =
//      'assets/images/background/yourbgname.png';
  static const String NotificationBg =
      'assets/images/background/notification.png';

  ///FLARE
  static const String PhoneFlare = "assets/flare/otp.flr";

  ///TRANSLATION
  static const String TranslationPath = "assets/i18n";
}
