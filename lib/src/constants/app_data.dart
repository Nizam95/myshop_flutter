import 'dart:ui';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:myshop_flutter/src/constants/app_constant.dart';
import 'package:myshop_flutter/src/models/marker_model.dart';
import 'package:myshop_flutter/src/models/place_model.dart';
import 'package:myshop_flutter/src/models/widget_list_model.dart';

class StaticListData {
  static final List<String> gender = ['MALE', 'FEMALE'];
  static final List<String> maritalStatus = ['SINGLE', 'MARRIED', 'DIVORCE'];

  static final List<Locale> languageOptions = [
    Locale('en', ''), // English, no country code
  ];

  static final List<PlaceModel> history = [
    PlaceModel(
        name: 'H&M NU Sentral',
        country: 'Malaysia',
        state: 'Kuala Lumpur',
        geometry: GeometryModel(
            latitude: '3.132867', longitude: '101.687321', type: 'points')),
    PlaceModel(
        name: 'Harvey Norman Nu Sentral',
        country: 'Malaysia',
        state: 'Kuala Lumpur',
        geometry: GeometryModel(
            latitude: '3.132867', longitude: '101.686552', type: 'points')),
    PlaceModel(
        name: 'Mi Store Nu Sentral KL',
        country: 'Malaysia',
        state: 'Kuala Lumpur',
        geometry: GeometryModel(
            latitude: '3.132900', longitude: '101.686519', type: 'points')),
    PlaceModel(
        name: 'The Body Shop (Nu Sentral)',
        country: 'Malaysia',
        state: 'Kuala Lumpur',
        geometry: GeometryModel(
            latitude: '3.132886', longitude: '101.686558', type: 'points')),
  ];

  static final List<MarkerModel> markerList = [
//    MarkerModel(
//        latLng: LatLng(0, 0),
//        phoneNumber: "",
//        name: "",
//        address: "",
//        description: "",
//        websiteUrl: ""),
    MarkerModel(
        latLng: LatLng(3.133708, 101.687321),
        phoneNumber: "03-2276 5650",
        name: "H&M NU Sentral",
        address:
            "L1 - 13, CC - 23, 201, Jalan Tun Sambanthan, Brickfields, 50470 Kuala Lumpur",
        description: "",
        websiteUrl: "https://www2.hm.com/en_asia4/index.html"),
    MarkerModel(
        latLng: LatLng(3.132867, 101.686552),
        phoneNumber: "03-2260 7866",
        name: "Harvey Norman Nu Sentral",
        address:
            "KL Sentral No. 201, Jalan Tun Sambathan Unit L3.01, Nu Sentral Mall, 50470 Kuala Lumpur",
        description: "",
        websiteUrl: "https://www.harveynorman.com.my/"),
    MarkerModel(
        latLng: LatLng(3.132900, 101.686519),
        phoneNumber: "03-2276 2911",
        name: "Mi Store Nu Sentral KL",
        address:
            "L2-26, NU Sentral, Sentral, Jalan Stesen Sentral 5, Brickfields, 50470 Kuala Lumpur, Federal Territory of Kuala Lumpur",
        description: "",
        websiteUrl: "https://www.mi.com/my/"),
    MarkerModel(
        latLng: LatLng(3.132865, 101.686548),
        phoneNumber: "012-616 0223",
        name: "Fipper Nu Sentral",
        address:
            "L2-27, NU Sentral, Jalan Stesen Sentral 5, Brickfields, Wilayah Persekutuan, 50470 Kuala Lumpur",
        description: "",
        websiteUrl: "https://www.fipperslipper.com/"),
    MarkerModel(
        latLng: LatLng(3.132886, 101.686558),
        phoneNumber: "012-536 8700",
        name: "The Body Shop (Nu Sentral)",
        address:
            "GF 30, NU Sentral, Jalan Stesen Sentral 5, 50470 Kuala Lumpur, Federal Territory of Kuala Lumpur",
        description: "",
        websiteUrl: "https://www.thebodyshop.com.my/"),
    MarkerModel(
        latLng: LatLng(3.133503, 101.687724),
        phoneNumber: "03-2276 5052",
        name: "UNIQLO Nu Sentral",
        address:
            "セントラル Ground 19 & 20, Jalan Travers, 50470 Kuala Lumpur, Wilayah Persekutuan Kuala Lumpur",
        description: "",
        websiteUrl: "https://www.uniqlo.com/my/"),
    MarkerModel(
        latLng: LatLng(3.133669, 101.686840),
        phoneNumber: "03-2276 0772",
        name: "Levi's NU Sentral",
        address:
            "Lot G02 & G03, NU Sentral, Jalan Tun Sambathan, Kuala Lumpur, 50470 Wilayah Persekutuan",
        description: "",
        websiteUrl: "https://www.levi.com.my/"),
    MarkerModel(
        latLng: LatLng(3.133681, 101.687821),
        phoneNumber: "03-2276 4141",
        name: "Adidas Store Kuala Lumpur Nu Sentral Mall",
        address:
            "L2-04, Nu Sentral, 201, Jalan Tun Sambanthan, Brickfields, 50470 Kuala Lumpur, Federal Territory of Kuala Lumpur",
        description: "",
        websiteUrl: "https://www.adidas.com.my/en"),
    MarkerModel(
        latLng: LatLng(3.133800, 101.687615),
        phoneNumber: "03-2276 2765",
        name: "MPH NU Sentral",
        address:
            "Lot 13 & 14, Level 2, Nu Sentral Mall, Jalan Tun Sambanthan, 50470 Kuala Lumpur",
        description: "",
        websiteUrl: "https://www.mphonline.com/en/home"),
    MarkerModel(
        latLng: LatLng(3.132879, 101.687115),
        phoneNumber: "03-2272 1142",
        name: "MyNews.com Nu Sentral",
        address:
            "LG34 Lower Ground Floor Nu Sentral Jln Tun Sambathan, Brickfields, Kuala Lumpur Sentral, 50470 Kuala Lumpur",
        description: "",
        websiteUrl: "http://www.mynews.com.my/products.php"),
    MarkerModel(
        latLng: LatLng(3.133430, 101.687577),
        phoneNumber: "016-299 2138",
        name: "Machines NU Sentral",
        address:
            "CC 35 & 36, Level Concourse Floor Nu Sentral Shopping Centre, Jalan Tun Sambanthan, Brickfields, 50470 Kuala Lumpur",
        description: "",
        websiteUrl: "https://www.machines.com.my/"),
    MarkerModel(
        latLng: LatLng(3.132836, 101.686548),
        phoneNumber: "019-764 4833",
        name: "Al-ikhsan Nu Sentral",
        address:
            "L3-15, L3-16, L3-17, NU Sentral, Brickfields, Wilayah Persekutuan, Jalan Stesen Sentral 5, Kuala Lumpur Sentral, 50470 Kuala Lumpur, Federal Territory of Kuala Lumpur",
        description: "",
        websiteUrl: "https://www.al-ikhsan.com/"),
  ];
}

class WidgetData {
  static final List<WidgetListModel> menu = [
    WidgetListModel(
        title: AppConstant.Home,
//        icon: AppTheme.bodyIcon(asset: AssetsPath.Home),
        value: 0,
        route: null),
  ];
}

class DefaultData {
  static final Duration defaultFetchDuration = Duration(seconds: 30);
  static final String defaultLanguage = 'en';
}
