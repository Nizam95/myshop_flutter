class RoutePaths {
  RoutePaths._();

  ///MAIN
  static const String MainPage = '/';
  static const String LogoutPage = '/logout';
  static const String LoginPage = '/login';

  ///SPLASH
  static const String SplashScreenPage = '/splash-screen';

  ///SUB MAIN

  ///COMMON
  static const String WebPageViewer = '/web-view';

  ///EXTERNAL
  static const String AppPlayStore = '/app-play-store';

  ///DRAWER
  static const String NotificationPage = '/notification';
  static const String ChangePasswordPage = '/change-password';
  static const String ProfilePage = '/profile';

  ///PUBLIC
  static const String VerifyPhonePage = '/verify-phone';
  static const String RegisterPage = '/register';
}
