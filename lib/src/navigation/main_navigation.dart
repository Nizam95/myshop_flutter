import 'package:animations/animations.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:myshop_flutter/src/constants/app_theme_constant.dart';
import 'package:myshop_flutter/src/controllers/main_controller.dart';
import 'package:myshop_flutter/src/widgets/drawer_widget.dart';
import 'package:myshop_flutter/src/views/main/home/home_view.dart';

class MainNavigation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child: GetBuilder<MainController>(
          init: MainController(),
          builder: (_) {
            return Scaffold(
              backgroundColor: Colors.transparent,
              drawer: DrawerWidget(),
              appBar: AppTheme.defaultAppBar,
//              bottomNavigationBar: Obx(
//                () => BottomNavigationBar(
//                    backgroundColor: Colors.white,
//                    currentIndex: _.selectedBottomNavigationIndex.value,
//                    showSelectedLabels: true,
//                    showUnselectedLabels: true,
//                    selectedItemColor: AppTheme.textPrimaryColor,
//                    unselectedItemColor: CupertinoColors.inactiveGray,
//                    selectedLabelStyle: TextStyle(
//                        color: AppTheme.textPrimaryColor,
//                        fontWeight: FontWeight.bold),
//                    unselectedLabelStyle: TextStyle(
//                        color: Colors.black, fontWeight: FontWeight.w500),
//                    type: BottomNavigationBarType.fixed,
//                    onTap: (int index) => _.setSelectedBottomNavigation(index),
//                    items: _.bottomNavigationItems),
//              ),
              body: PageTransitionSwitcher(
                transitionBuilder: (
                  Widget child,
                  Animation<double> animation,
                  Animation<double> secondaryAnimation,
                ) {
                  return FadeThroughTransition(
                    animation: animation,
                    secondaryAnimation: secondaryAnimation,
                    child: child,
                  );
                },
                child: HomePage(),
              ),
            );
          }),
    );
  }
}
