import 'package:catcher/core/catcher.dart';
import 'package:myshop_flutter/src/constants/app_constant.dart';
import 'package:myshop_flutter/src/constants/enum_constant.dart';
import 'package:myshop_flutter/src/services/flavor_service.dart';
import 'app.dart';
import 'init.dart';

void main() async {
  await init();

  ///uncomment this to allow http override (fix http callback error on emulator)
//  HttpOverrides.global = new CustomHttpOverrides();

  //Setup environment
  FlavorService(
      flavor: Flavor.staging,
      values: FlavorValues(
        baseUrl: "https://yourapi", //required
        url: "yourapi", //required (baseUrl without http tag)
//        imageUrl: "image url", //optional
//        addressUrl: "address url", //optional
        reCaptchaKey: "your recaptcha key", //google recaptcha KEY
      ));

  //initialize app
  Catcher(
    App(),
    debugConfig: CatcherConfig.debugOptions,
    releaseConfig: CatcherConfig.releaseOptions,
  );
}
