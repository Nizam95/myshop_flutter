import 'package:get_it/get_it.dart';
import 'package:myshop_flutter/src/services/push_notification_service.dart';
import 'package:myshop_flutter/src/services/shared_preferences_service.dart';

GetIt locator = GetIt.instance;

Future setupLocator() async {
  var instance = await SharedPreferencesService.getInstance();
  locator.registerSingleton<SharedPreferencesService>(instance);
  locator.registerLazySingleton(() => PushNotificationService());
}
